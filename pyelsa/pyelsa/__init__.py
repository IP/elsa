from .pyelsa_core import *
from .pyelsa_functionals import *
from .pyelsa_generators import *
from .pyelsa_io import *
from .pyelsa_operators import *
from .pyelsa_proximal_operators import *
from .pyelsa_line_search import *
from .pyelsa_solvers import *
from .pyelsa_projectors import *
from .visualizer import visualize_geometry, visualize_reconstruction
from .axdt import setupProblem, extractFibers

try:
    from .pyelsa_projectors_cuda import *

    importedCudaProjectors = True
except ModuleNotFoundError:
    # print("pyelsa not build with CUDA projector support")
    importedCudaProjectors = False


def cudaProjectorsEnabled():
    return importedCudaProjectors
