#include <pybind11/pybind11.h>
#include <pybind11/complex.h>
#include <pybind11/eigen.h>
#include <pybind11/stl.h>

#include "BlockLinearOperator.h"
#include "FunkRadonTransform.h"
#include "SphericalHarmonicsTransform.h"
#include "Scaling.h"
#include "AXDTOperator.h"
#include "XGIDetectorDescriptor.h"
#include "SphericalCoefficientsDescriptor.h"
#include "WeightingFunction.h"
#include "Math.hpp"
#include "SphericalPositivity.h"

namespace py = pybind11;

void add_axdt_operator(py::module& a)
{
    using namespace elsa;
    using namespace elsa::axdt;

    using SphCoeffsDesc = SphericalCoefficientsDescriptor;

    py::class_<AXDTOperator<float>, LinearOperator<float>>(a, "AXDTOperatorf")
        .def(py::init<const SphCoeffsDesc&, const XGIDetectorDescriptor&,
                      const LinearOperator<float>&, std::optional<DataContainer<float>>>(),
             py::arg("domainDescriptor"), py::arg("rangeDescriptor"), py::arg("projector"),
             py::arg("weights") = std::nullopt);

    py::class_<AXDTOperator<double>, LinearOperator<double>>(a, "AXDTOperatord")
        .def(py::init<const SphCoeffsDesc&, const XGIDetectorDescriptor&,
                      const LinearOperator<double>&, std::optional<DataContainer<double>>>(),
             py::arg("domainDescriptor"), py::arg("rangeDescriptor"), py::arg("projector"),
             py::arg("weights") = std::nullopt);

    a.attr("AXDTOperator") = a.attr("AXDTOperatorf");
}

namespace detail
{
    using namespace elsa;
    using namespace elsa::axdt;

    using Edge = std::pair<index_t, index_t>;

    template <typename data_t>
    inline std::vector<char> localMaxima(const Vector_t<data_t>& ODF,
                                         const std::vector<Edge>& edges)
    {
        // All nodes are initialized as local maxima
        std::vector<char> isMax(ODF.size(), true);

        // Whenever there is (i, j) in E with eta(i) < eta(j), then i is not a local maximum
        for (const auto& [i, j] : edges) {

            const auto etaI = ODF[i];
            const auto etaJ = ODF[j];

            if (etaI < etaJ) {
                isMax[i] = false;
            } else if (etaI > etaJ) {
                isMax[j] = false;
            }
        }

        return isMax;
    }

    template <typename data_t>
    inline std::vector<index_t> getSortedIndices(const Vector_t<data_t>& ODF,
                                                 const std::vector<char>& isMax,
                                                 const index_t maximaCount)
    {
        std::vector<index_t> indices;
        for (index_t i = 0; i < asSigned(isMax.size()); i++) {
            if (isMax[i]) {
                indices.push_back(i);
            }
        }

        // Sort indices descending by magnitude
        std::partial_sort(indices.begin(), indices.begin() + maximaCount, indices.end(),
                          [&ODF](index_t a, index_t b) { return ODF[a] > ODF[b]; });

        return indices;
    }

    template <typename data_t>
    inline std::vector<index_t> getUniqueIndices(const std::vector<index_t>& indices,
                                                 const axdt::DirVecList<data_t>& directions)
    {

        // Remove duplicates based on angle heuristic
        // We discard any vertices closer than 10deg to another point already added
        // TODO: Seems arbitrary to me!
        // Also we restrict ourselves to the upper hemisphere as scattering is symmetric under
        // parity
        constexpr float cos10deg = 0.984807753;

        std::vector<index_t> uniqueIndices;

        // TODO: Doesn't this induce a bias depending on the order of given sampling directions?
        // Maybe just exclude neighbouring directions?
        for (auto i : indices) {
            if (std::none_of(uniqueIndices.begin(), uniqueIndices.end(), [&](index_t j) {
                    auto cosTheta = directions[j].dot(directions[i]);
                    return abs(cosTheta) > cos10deg; // cos decreases monotonically
                })) {
                uniqueIndices.push_back(i);
            }
        }
        return uniqueIndices;
    }

    // Generalized Fractional Anisotropy represents a quality measure
    // cf. J. Cohen-Adad, M. Descoteaux, S. Rossignol, R. D. Hoge, R. Deriche, and H. Benali.
    // “Detection of multiple pathways in the spinal cord using q-ball imaging”.
    // DOI: 10.1016/j.neuroimage.2008.04.243
    // URL: http://linkinghub.elsevier.com/retrieve/pii/S1053811908005119.
    // TODO: Maybe rewrite this using thrust reductions/transforms
    template <typename data_t>
    DataContainer<data_t> gfa(const DataContainer<data_t>& sphericalCoeffs)
    {

        auto gfa = emptylike<data_t>(sphericalCoeffs.getBlock(0));

        const auto volumeSize = sphericalCoeffs.getBlock(0).getSize();

#pragma omp parallel for
        for (index_t volIdx = 0; volIdx < volumeSize; volIdx++) {

            Eigen::Map<const Vector_t<data_t>, Eigen::Unaligned, Eigen::InnerStride<Eigen::Dynamic>>
                viewCoeffs{thrust::raw_pointer_cast(sphericalCoeffs.storage().data()) + volIdx,
                           sphericalCoeffs.getNumberOfBlocks(), volumeSize};

            gfa[volIdx] = std::sqrt(1 - viewCoeffs[0] * viewCoeffs[0] / viewCoeffs.squaredNorm());
        }
        return gfa;
    }

    // Find local maxima of a function given as spherical harmonics coefficients by evaluation
    // on sampling directions
    template <typename data_t>
    std::tuple<DataContainer<data_t>, DataContainer<index_t>>
        extractFibers(const DataContainer<data_t>& sphericalCoeffs,
                      const DirVecList<data_t>& directions, const std::vector<Edge>& edges,
                      const index_t maximaCount, bool clip)
    {
        using SpreadVector_t = Eigen::Map<const Vector_t<data_t>, Eigen::Unaligned,
                                          Eigen::InnerStride<Eigen::Dynamic>>;

        const auto& etaDesc = downcast_safe<const SphericalCoefficientsDescriptor>(
            sphericalCoeffs.getDataDescriptor());
        const auto& volDesc = etaDesc.getDescriptorOfBlock(0);

        const auto dirCount = asSigned(directions.size());
        const auto volumeSize = volDesc.getNumberOfCoefficients();

        const VolumeDescriptor dirSpaceDesc({dirCount});
        const VolumeDescriptor coeffSpaceDesc({etaDesc.getNumberOfBlocks()});

        const auto sphericalHarmonics =
            evalSphericalHarmonics<data_t>(etaDesc.symmetry, etaDesc.degree, directions);

        const auto funkRadon = funkRadonMatrix<data_t>(etaDesc.symmetry, etaDesc.degree);

        // Initialize magnitude with zeros such that missing maxima are irrelevant

        auto magnitude = zeros<data_t>(IdenticalBlocksDescriptor{maximaCount, volDesc});
        auto fiberIndices = empty<index_t>(IdenticalBlocksDescriptor{maximaCount, volDesc});

#pragma omp for schedule(runtime)
        for (index_t volIdx = 0; volIdx < volumeSize; volIdx++) {

            SpreadVector_t viewOriginalCoeffs{
                thrust::raw_pointer_cast(sphericalCoeffs.storage().data()) + volIdx,
                sphericalCoeffs.getNumberOfBlocks(),
                Eigen::InnerStride<Eigen::Dynamic>{volumeSize}};

            // Even though this is less readable, this way we don't having to allocate a temporary
            // Vector_t of modified coefficients just to make the type inference for the expression
            // templates work...
            // Writing this like this allows us to defer the eval to the end, at which point the
            // types converge to Vector_t
            Vector_t<data_t> orientationDensity = [&]() {
                if (clip) {
                    // Clamp negative values to zero before Funk-Radon transform...
                    const auto coeffs = sphericalHarmonics.transpose()
                                        * (sphericalHarmonics * viewOriginalCoeffs).cwiseMax(0);

                    return (sphericalHarmonics * funkRadon * coeffs).eval();
                } else
                    return (sphericalHarmonics * funkRadon * viewOriginalCoeffs).eval();
            }();

            auto isMax = localMaxima(orientationDensity, edges);

            auto maxIndices = getSortedIndices(orientationDensity, isMax, maximaCount);

            // auto uniqueIndices = getUniqueIndices<data_t>(maxIndices, directions);
            const auto& uniqueIndices =
                maxIndices; // Ignore uniqueness as we often only take first fiber!!!

            // Store the localMax values in descending order up to the maximaCount
            // Note that r and i have shape (volume x maximaCount)
            index_t detectedMaxima = uniqueIndices.size();

            for (index_t fiber = 0; fiber < std::min(maximaCount, detectedMaxima); fiber++) {
                magnitude[fiber * volumeSize + volIdx] = orientationDensity[uniqueIndices[fiber]];
                fiberIndices[fiber * volumeSize + volIdx] = uniqueIndices[fiber];
            }
        }

        return {magnitude, fiberIndices};
    }

} // namespace detail

void add_weighting_functions(py::module& a)
{
    using namespace elsa::axdt;

    a.def("numericalWeightingFunctionf", &numericalWeightingFunction<float>,
          py::arg("rangeDescriptor"), py::arg("symmetry"), py::arg("degree"), py::arg("directions"),
          py::return_value_policy::move);
    a.def("numericalWeightingFunctiond", &numericalWeightingFunction<double>,
          py::arg("rangeDescriptor"), py::arg("symmetry"), py::arg("degree"), py::arg("directions"),
          py::return_value_policy::move);

    a.def("exactWeightingFunctionf", &exactWeightingFunction<float>, py::arg("rangeDescriptor"),
          py::arg("symmetry"), py::arg("degree"), py::return_value_policy::move);
    a.def("exactWeightingFunctiond", &exactWeightingFunction<double>, py::arg("rangeDescriptor"),
          py::arg("symmetry"), py::arg("degree"), py::return_value_policy::move);

    a.attr("numericalWeightingFunction") = a.attr("numericalWeightingFunctionf");
    a.attr("exactWeightingFunction") = a.attr("exactWeightingFunctionf");
}

void add_visu_helpers(py::module& a)
{
    using namespace detail;
    a.def("extractFibersf", &extractFibers<float>, py::arg("sphericalCoeffs"),
          py::arg("directions"), py::arg("edges"), py::arg("maximaCount"), py::arg("clip"),
          py::return_value_policy::move);
    a.def("extractFibersd", &extractFibers<double>, py::arg("sphericalCoeffs"),
          py::arg("directions"), py::arg("edges"), py::arg("maximaCount"), py::arg("clip"),
          py::return_value_policy::move);

    a.attr("extractFibers") = a.attr("extractFibersf");
}

void add_AXDT(py::module& axdt)
{
    using namespace elsa;
    using namespace elsa::axdt;

    py::enum_<Symmetry>(axdt, "Symmetry")
        .value("Even", Symmetry::even)
        .value("Regular", Symmetry::regular);

    add_axdt_operator(axdt);
    add_weighting_functions(axdt);
    add_visu_helpers(axdt);
}

PYBIND11_MODULE(pyelsa_axdt, m)
{
    add_AXDT(m);
}
