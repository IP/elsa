stages:
- docker-build
- static-test
- compile
- test
- sanitizer
- coverage
- docs

### workflow ###

# Conditions in plain english:
# - the pipeline
# - Create a pipeline if, the pipeline is a merge request pipeline, or
# - Do not create a pipeline, if a branch pipeline is triggered, but an open merge request exists for that branch, or
# - Create a pipeline if a branch pipeline is triggered, without an associated open merge requests
# based on: https://docs.gitlab.com/ee/ci/yaml/workflow.html#switch-between-branch-pipelines-and-merge-request-pipelines
workflow:
  rules:
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS && $CI_PIPELINE_SOURCE == "push"
      when: never
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      when: never
    - if: $CI_COMMIT_BRANCH

### workflow ###

# Conditions in plain english:
# - the pipeline
# - Create a pipeline if, the pipeline is a merge request pipeline, or
# - Do not create a pipeline, if a branch pipeline is triggered, but an open merge request exists for that branch, or
# - Create a pipeline if a branch pipeline is triggered, without an associated open merge requests
# based on: https://docs.gitlab.com/ee/ci/yaml/workflow.html#switch-between-branch-pipelines-and-merge-request-pipelines
workflow:
  rules:
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS && $CI_PIPELINE_SOURCE == "push"
      when: never
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      when: never
    - if: $CI_COMMIT_BRANCH

### Cache setup ###

# Caches should only be used for caching between pipelines not jobs
# By default, have a unique and separate cache for each branch and job in the CI, otherwise it can happen that
# different build configurations are spilled into different jobs and sporadic build failure occurs.
cache:
  key: "$CI_COMMIT_REF_SLUG-$CI_JOB_STAGE-$CI_JOB_NAME"
  paths:
    - build/_deps/*

### job templates ###

#### Schedule jobs ####

# run job if connected to a schedule, merge request or some other things
.job_template: &run_on_merge_requests
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_COMMIT_BRANCH == "master" && $CI_PIPELINE_SOURCE == "schedule"'
    - when: never

#### Build job templates ####

.job_template: &build_job_artifact
  stage: compile
  artifacts:
    name: "$CI_JOB_NAME-$CI_COMMIT_REF_NAME"
    paths:
      # The actual build artifacts
      - build/bin
      - build/lib
      - build/pyelsa/
      - build/elsa/elsaConfig.cmake
      - build/elsa/elsaConfigVersion.cmake

      # If we ever generate headers, store them as well
      - build/elsa/**/*.h

      # CTest files
      - build/**/CTestTestfile.cmake
      - build/**/tests/test_*include-*.cmake
      - build/**/tests/test_*tests-*.cmake
      - build/Testing
      - build/junit_report.xml

      - install-elsa/include/
      - install-elsa/lib/
    exclude:
      - build/_deps/*
    expire_in: 60 mins

.buildjob_script_normal:
  script: &buildjob_normal
    - mkdir -p build
    - cd build
    - if [ $COMPILER == "clang" ]; then CMAKE_EXTRA_ARGS="-DCMAKE_CXX_FLAGS=\"-stdlib=libc++\" -DCMAKE_EXE_LINKER_FLAGS=\"-lc++abi\""; fi;
    - if [ $COMPILER == "nvcc" ]; then CMAKE_EXTRA_ARGS="-DThrust_DIR=$Thrust_DIR -DELSA_CUDA_VECTOR=ON -DELSA_BENCHMARKS=ON"; fi;
    - echo $CMAKE_EXTRA_ARGS
    - cmake .. -GNinja -DELSA_BENCHMARKS=ON -DCMAKE_INSTALL_PREFIX="../install-elsa" $CMAKE_EXTRA_ARGS
    - ninja
    - ninja build-tests
    - ninja build-examples
    - ninja install

#### Test job templates ####

.testjob_template: &test_job_artifact
  script:
    - cd build
    - ctest --schedule-random --output-junit junit_report.xml || ctest --rerun-failed --output-on-failure
  artifacts:
    name: "$CI_COMMIT_REF_SLUG-$CI_JOB_NAME"
    paths:
      - build/
    expire_in: 60 mins
    reports:
      junit: "build/junit_report.xml"

#### Install job templates ####

.installjob_anchors: &install_job
  <<: *run_on_merge_requests
  script:
    - cd testing
    - mkdir -p build/ && cd build/
    - echo $CMAKE_EXTRA_ARGS
    - cmake .. -GNinja -DELSA_INSTALL_DIR="$CI_PROJECT_DIR/install-elsa" $CMAKE_EXTRA_ARGS
    - ninja

### static test ###

clang-format:
  stage: static-test
  image: $CI_REGISTRY/tum-ciip/elsa/clang:14
  script:
    ./tools/ci_scripts/clang-format-test.sh
  tags:
    - linux
    - elsa
    - clang

clang-tidy:
  stage: static-test
  image: $CI_REGISTRY/tum-ciip/elsa/clang:14
  variables:
    Thrust_DIR: /usr/local/cuda/targets/x86_64-linux/lib/cmake/thrust
  script:
    ./tools/ci_scripts/clang-tidy.sh
  allow_failure: true
  tags:
    - linux
    - elsa
    - cuda

comment-formating:
  stage: static-test
  image: $CI_REGISTRY/tum-ciip/elsa/clang:14
  script:
    ./tools/ci_scripts/check-comment-style.sh
  allow_failure: true
  tags:
    - linux
    - elsa
    - clang

cmake-format:
  stage: static-test
  image: $CI_REGISTRY/tum-ciip/elsa/clang:14
  script:
    ./tools/ci_scripts/cmake-format-test.sh
  tags:
    - linux
    - elsa
    - clang

cmake-lint:
  stage: static-test
  image: $CI_REGISTRY/tum-ciip/elsa/clang:14
  script:
    ./tools/ci_scripts/cmake-lint-test.sh
  tags:
    - linux
    - elsa
    - clang

### compile jobs ###

# Do a matrix here as well
build-pybind:
  <<: *build_job_artifact
  image: $CI_REGISTRY/tum-ciip/elsa/pybind:clang-14
  script:
    - pip install --verbose .
    - pip install -U pytest
    - pytest
    - python examples/solver/example2d.py --size 16 --no-show
  tags:
    - linux
    - elsa

build-pybind-cuda:
  <<: *build_job_artifact
  image: $CI_REGISTRY/tum-ciip/elsa/cuda:11.7.1
  variables:
    Thrust_DIR: /usr/local/cuda/targets/x86_64-linux/lib/cmake/thrust
  script:
    - pip install --verbose .
    - pip install -U pytest
    - pytest
    - python examples/solver/example2d.py --size 16 --no-show
  tags:
    - linux
    - elsa
    - cuda

build-clang:
  <<: *build_job_artifact
  image: $CI_REGISTRY/tum-ciip/elsa/pybind:$COMPILER-$COMPILER_VERSION
  script:
  - *buildjob_normal
  variables:
    COMPILER: clang
  parallel:
    matrix:
      - COMPILER_VERSION: [10, 11, 12, 13]
  tags:
    - linux
    - elsa
    - $COMPILER

# TODO: find out what is wrong and fix it! I want to build on GCC
# build-gcc:
#   <<: *build_job_artifact
#   image: $CI_REGISTRY/tum-ciip/elsa/gcc:$COMPILER_VERSION
#   script:
#   - *buildjob_normal
#   variables:
#     COMPILER: gcc
#   parallel:
#     matrix:
#       - COMPILER_VERSION: [11, 12]
#   tags:
#     - linux
#     - elsa
#     - $COMPILER

build-cuda:
  <<: *build_job_artifact
  image: $CI_REGISTRY/tum-ciip/elsa/cuda:$CUDA_VERSION
  variables:
    Thrust_DIR: /usr/local/cuda/targets/x86_64-linux/lib/cmake/thrust
  script:
    - *buildjob_normal
  parallel:
    matrix:
      - CUDA_VERSION: [12.2.2, 12.1.1, 12.0.1, 11.8.0, 11.7.1]
        COMPILER: nvcc
  tags:
    - linux
    - elsa
    - gcc
    - cuda

### test jobs ###

.test-compiler:
  <<: *test_job_artifact
  stage: test
  image: $CI_REGISTRY/tum-ciip/elsa/$IMAGE
  tags:
    - linux
    - elsa
    - $COMPILER

# TODO: find out what is wrong and fix it! I want to build on GCC
# .test-gcc:
#   extends: .test-compiler
#   variables:
#     COMPILER: gcc
#     IMAGE: $COMPILER:$COMPILER_VERSION

.test-clang:
  extends: .test-compiler
  variables:
    COMPILER: clang
    IMAGE: pybind:$COMPILER-$COMPILER_VERSION

.test-cuda:
  <<: *test_job_artifact
  stage: test
  image: $CI_REGISTRY/tum-ciip/elsa/cuda:$CUDA_VERSION
  tags:
    - linux
    - elsa
    - gcc
    - cuda

# At the time of writing, variables from the parallel:matrix can not be used in the dependency list
# see https://gitlab.com/gitlab-org/gitlab/-/merge_requests/82734 and
# https://forum.gitlab.com/t/ci-specifying-artifact-dependencies-when-using-parallel-matrix/45026/2
# TODO: Once this is implemented use it!
# test-gcc12:
#   extends: .test-gcc
#   dependencies:
#     - "build-gcc: [12]"
#   variables:
#     COMPILER_VERSION: 12

test-clang10:
  extends: .test-clang
  dependencies:
    - "build-clang: [10]"
  variables:
    COMPILER_VERSION: 10

test-clang11:
  extends: .test-clang
  dependencies:
    - "build-clang: [11]"
  variables:
    COMPILER_VERSION: 11

test-clang12:
  extends: .test-clang
  dependencies:
    - "build-clang: [12]"
  variables:
    COMPILER_VERSION: 12

test-clang13:
  extends: .test-clang
  dependencies:
    - "build-clang: [13]"
  variables:
    COMPILER_VERSION: 13

test-cuda-12.2:
  extends: .test-cuda
  dependencies:
    - "build-cuda: [12.2.2, nvcc]"
  variables:
    CUDA_VERSION: 12.2.2

test-cuda-12.1:
  extends: .test-cuda
  dependencies:
    - "build-cuda: [12.1.1, nvcc]"
  variables:
    CUDA_VERSION: 12.1.1

test-cuda-12.0:
  extends: .test-cuda
  dependencies:
    - "build-cuda: [12.0.1, nvcc]"
  variables:
    CUDA_VERSION: 12.0.1

test-cuda-11.8:
  extends: .test-cuda
  dependencies:
    - "build-cuda: [11.8.0, nvcc]"
  variables:
    CUDA_VERSION: 11.8.0

test-cuda-11.7:
  extends: .test-cuda
  dependencies:
    - "build-cuda: [11.7.1, nvcc]"
  variables:
    CUDA_VERSION: 11.7.1

### Install jobs ###

.install-compiler:
  <<: *install_job
  stage: test
  image: $CI_REGISTRY/tum-ciip/elsa/$IMAGE
  tags:
    - linux
    - elsa
    - $COMPILER

# .install-gcc:
#   extends: .install-compiler
#   variables:
#     COMPILER: gcc
#     IMAGE: $COMPILER:$COMPILER_VERSION

.install-clang:
  extends: .install-compiler
  variables:
    COMPILER: clang
    IMAGE: pybind:$COMPILER-$COMPILER_VERSION
    CMAKE_EXTRA_ARGS: "-DCMAKE_CXX_FLAGS=\"-stdlib=libc++\" -DCMAKE_EXE_LINKER_FLAGS=\"-lc++abi\""

.install-cuda:
  <<: *install_job
  stage: test
  variables:
    CMAKE_EXTRA_ARGS: "-DThrust_DIR=/usr/local/cuda/lib64/cmake/thrust/ -DELSA_CUDA_VECTOR=ON"
  image: $CI_REGISTRY/tum-ciip/elsa/cuda:$CUDA_VERSION
  script:
    - cd testing
    - mkdir -p build/ && cd build/
    - echo $CMAKE_EXTRA_ARGS
    - cmake .. -GNinja -DELSA_INSTALL_DIR="$CI_PROJECT_DIR/install-elsa" $CMAKE_EXTRA_ARGS
    - ninja
  tags:
    - linux
    - elsa
    - gcc
    - cuda

# install-gcc12:
#   extends: .install-gcc
#   dependencies:
#     - "build-gcc: [12]"
#   variables:
#     COMPILER_VERSION: 12

install-clang10:
  extends: .install-clang
  dependencies:
    - "build-clang: [10]"
  variables:
    COMPILER_VERSION: 10

install-clang11:
  extends: .install-clang
  dependencies:
    - "build-clang: [11]"
  variables:
    COMPILER_VERSION: 11

install-clang12:
  extends: .install-clang
  dependencies:
    - "build-clang: [12]"
  variables:
    COMPILER_VERSION: 12

install-clang13:
  extends: .install-clang
  dependencies:
    - "build-clang: [13]"
  variables:
    COMPILER_VERSION: 13

install-cuda-11.7:
  extends: .install-cuda
  dependencies:
    - "build-cuda: [11.7.1, nvcc]"
  variables:
    CUDA_VERSION: 11.7.1

install-cuda-11.8:
  extends: .install-cuda
  dependencies:
    - "build-cuda: [11.8.0, nvcc]"
  variables:
    CUDA_VERSION: 11.8.0

install-cuda-12.0:
  extends: .install-cuda
  dependencies:
    - "build-cuda: [12.0.1, nvcc]"
  variables:
    CUDA_VERSION: 12.0.1

install-cuda-12.1:
  extends: .install-cuda
  dependencies:
    - "build-cuda: [12.1.1, nvcc]"
  variables:
    CUDA_VERSION: 12.1.1

install-cuda-12.2:
  extends: .install-cuda
  dependencies:
    - "build-cuda: [12.2.2, nvcc]"
  variables:
    CUDA_VERSION: 12.2.2

### sanitizers ###

# .cuda-memcheck:
#   <<: *run_on_merge_requests
#   stage: sanitizer
#   image: $CI_REGISTRY/tum-ciip/elsa/cuda:$CUDA_VERSION
#   script:
#     - ./tools/ci_scripts/cuda-memcheck.sh
#   tags:
#     - linux
#     - elsa
#     - gcc
#     - cuda
#
# cuda-memcheck-11.7:
#   extends: .cuda-memcheck
#   dependencies:
#     - test-cuda-11.7
#   variables:
#     CUDA_VERSION: 11.7.0
#
# cuda-memcheck-11.6:
#   extends: .cuda-memcheck
#   dependencies:
#     - test-cuda-11.6
#   variables:
#     CUDA_VERSION: 11.6.2
#
# cuda-memcheck-11.5:
#   extends: .cuda-memcheck
#   dependencies:
#     - test-cuda-11.5
#   variables:
#     CUDA_VERSION: 11.5.2

# Be sure to run this job with container which has privilege mode set
asan-ubsan:
  <<: *run_on_merge_requests
  stage: sanitizer
  image: $CI_REGISTRY/tum-ciip/elsa/clang:14
  dependencies: []
  script:
    - mkdir -p build
    - cd build
    - CXX_FLAGS="-O1" cmake -GNinja -DWANT_CUDA=OFF -DELSA_BUILD_PYTHON_BINDINGS=OFF -DCMAKE_BUILD_TYPE=Debug -DELSA_SANITIZER="Address;Undefined" ..
    - ninja tests
  tags:
    - linux
    - elsa
    - gcc
    - cuda


### test coverage ###

# Enforce GCC for test coverage, as our coverage only works with gcov and acts weird with clang
test-coverage:
  <<: *run_on_merge_requests
  stage: coverage
  image: $CI_REGISTRY/tum-ciip/elsa/cuda:11.7.0
  dependencies: []
  variables:
    Thrust_DIR: /usr/local/cuda/targets/x86_64-linux/lib/cmake/thrust
  coverage: /^\s*lines.*:\s*\d+.\d+\%/
  script:
    - update-alternatives --install /usr/bin/llvm-cov llvm-cov /usr/bin/llvm-cov-12 10
    - update-alternatives --install /usr/bin/llvm-profdata llvm-profdata /usr/bin/llvm-profdata-12 10
    - mkdir -p build
    - cd build
    - CXX=clang++ cmake .. -GNinja -DWANT_CUDA=OFF -DThrust_DIR=$Thrust_DIR -DELSA_BUILD_PYTHON_BINDINGS=OFF -DCMAKE_BUILD_TYPE=Debug -DELSA_COVERAGE=ON
    - ninja build-tests
    - ninja ccov-all
  artifacts:
    name: "$CI_JOB_NAME-$CI_COMMIT_REF_NAME-coverage"
    paths:
      - build/ccov/
    expire_in: 60 mins
  tags:
    - linux
    - elsa
    - gcc


### deploy docs and coverage report ###

stage-docs:
  <<: *run_on_merge_requests
  stage: docs
  dependencies:
    - test-coverage
  script:
    - mkdir -p build
    - cd build
    - cmake .. -GNinja
    - ninja docs
    - mkdir -p /var/www/ciip/elsadocs-${CI_COMMIT_REF_SLUG}/
    - cp -r docs/sphinx/* /var/www/ciip/elsadocs-${CI_COMMIT_REF_SLUG}/
    - cd ..
    - mkdir -p /var/www/ciip/elsacoverage-${CI_COMMIT_REF_SLUG}/
    - cp -r build/ccov/all-merged/* /var/www/ciip/elsacoverage-${CI_COMMIT_REF_SLUG}/
  tags:
    - elsa-docs-deploy

deploy-docs:
  rules:
    - if: '$CI_COMMIT_BRANCH == "master" && $CI_PIPELINE_SOURCE == "schedule"'
  stage: docs
  dependencies:
    - test-coverage
  script:
    - mkdir -p build
    - cd build
    - cmake .. -GNinja
    - ninja docs
    - cp -r docs/sphinx/* /var/www/ciip/elsadocs/
    - cd ..
    - cp -r build/ccov/all-merged/* /var/www/ciip/elsacoverage/
  tags:
    - elsa-docs-deploy



### Docker jobs ###

include:
  - local: 'tools/docker/ci-build-script.yml'
