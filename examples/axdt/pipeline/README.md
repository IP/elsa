This directory contains several scripts related to reconstructing AXDT data.

Starting with raw data in the propietary format coming out of the scanners, you should first figure out the first image ID. This can then be passed to the [preprocessing script](preprocess.py) which is currently tailored to the Crossed Sticks dataset. It loads the projection data ($s_{n}$), masks it with a circular mask as the grating has this exact shape and then applies the fourier transform which yields the mean intensity $a$ and amplitude $b$. The same is done for the reference ('flat-field') images.

Calling it might look like:
```shell
./preprocess.py ~/share-all/e17-data/ddf/samples/tooth_irene_xtt_2/paxscan/ --begin_id 52364
```

While this outputs `.edf` files for (ff)[a,b] the downstream scripts also expect a `.yaml` file with metadata, typically called `dataset_dci.yaml`. These currently have to be adapted manually from existing datasets. Putting the preprocessed data and this file into a well-named directory is advisable.

For debugging purposes, we might want to look a the preprocessed projections (to prevent garbage-in-garbage-out problems). A simple slideable [viewer](./projections.py) helps us accomplish this:
```shell 
./projections.py -i data/input
```


As we now have all the necessary data to begin a dark-field reconstruction, you should choose a statistical noise model (and hence an associated loss we'll optimize). Some only use the dark-field coefficient $d = \frac{b}{a} / \frac{ffb}{ffa}$ while the more sophisticated Rician losses (i.e. noise models) rely on the dependence of $a$ and $b$. 
Based on Schilling et al. we distinguish the modes
- 0: Least Squares on the darkfield coefficients $\ln d$
- 1: Exponential Least Squares on $d$
- 2a: Approximated Joint Rician Likelihood ($a$, $b$)
- 2b: Exact Joint Rician Likelihood ($a$, $b$)

Since all reconstruction scripts allow for multiple modes to be specified, we can compare them by just crunching through all of them after one another. In terms of reconstruction methods, we currently have examples showcasing
- A [naive](reconstruct.py) optimization of each mode using FGM (either yielding the scattering strengths $\eta$ and $\mu$ for modes 2[a,b] or just $\mu$ for modes 0,1)
- A [sequential](sequential.py) or independent (--use-given-a) optimization that first solves for $\mu$ (using APGD and a non-negativity constraint) and then goes on to solve for $\eta$ using either the given mean intensities $a$ or the forwards-projected reconstruction image $\mu$
- A [constrained](constrained.py) optimization that enforces non-negativity of the evaluated scattering strengths: $\eta(u_k) \geq 0 \forall k$
- A [regularized](regularization.py) optimization applying an L1-loss promoting sparsity on either all or just the anisotropic spherical harmonics.

Save for problem-specific options all reconstruction scripts should be callable with a command like
```shell
./reconstruct.py data/input/ -b 4 -m 0 1 2a 2b -r data/output/ -i 1000 -e 100
```

where using a higher binning ratio is advisable for quick testing. The reconstructions get saved every -e steps as `.npy` files with a specific naming that captures the reconstruction parameters.

Visualizing the reconstructions is the last step in this pipeline. We currently offer three options:
- Plotting the evaluated scattering strength $\eta$ as [surfaces](./surfaces) in 3D
- Detecting local maxima ('fibers') of $\eta$ and plotting it as 3D [fibers](./fibers3d.py)
- Visualizing fibers in 2D along with optional attenuation data serving as background
Example invocations look like
```shell
./surfaces.py -c eta.npy -i -z 3
./fibers3d.py -c eta.npy -n 1 -z 3 -i 
./fibers2d.py -c eta.npy -n 1 -z 3 -i --mu mu.npy
```
As matplotlib employs CPU rendering, things tend get slow really fast, so I'd advise specifiying `-e k` to only treat every k-th voxel (cubically reducing the computational overhead).

Another bit that is work-in-progress is the ability to generate [synthetic data](./crime.py) (by committing the inverse crime). We don't have a fully-fledged physical simulation using e.g. GATE up and running, so we simply take the flat-field measurements from a given dataset (e.g. crossed sticks) and multiply forward-projected scattering functions to get pseudo-measurements. I plan to test the performance of our noise assumptions using Poisson noise after multiplication with an initial intensity parameter, as soon as I get the synthetic data to reconstruct reliably. This would then allow us to plot the minimum achievable loss after many iterations for each noise model, thus directly comparing their performance in different noise regimes. Generating a new data directory is as easy as calling
```shell
./crime.py data/input -o data/output
```

As we now potentially have ground-truth data to compare against, we should think about metrics to measure our reconstruction quality. [Currently](diffReco.py), we only take the L2 distance of the scattering functions evaluated on some sampling directions, but much more sophisticated and (robust!) metrics are needed in the near future:
```shell
./diffReco eta0.npy eta1.npy
```

While comparing analytical to numerical [weights](../../../elsa/operators/WeightingFunction.h), I needed a simple way to visualize the 15 or so coefficients weight for each detector pose. Paths to `.npy` files containing these are hardcoded in the script, so
```shell
./diffWeights.py
```
suffices.