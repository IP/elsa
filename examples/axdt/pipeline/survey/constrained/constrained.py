#!/usr/bin/env python3
import argparse
import csv
from pathlib import Path
from typing import Any, List, Optional, Tuple

import numpy as np
import yaml
from tqdm import trange
from timeit import default_timer as timer
from utils import *

import pyelsa as elsa

silence()


def arguments() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description='AXDT Reconstruction')

    parser.add_argument(
        "input", type=Path, help="Input directory")
    parser.add_argument("--config", "-c", type=Path,
                        default="dataset_dci.yaml", help="Name of YAML config file")

    parser.add_argument("--reco-dir", "-o", required=True, type=Path,
                        help="Path to store reconstruction at")

    parser.add_argument("--model", "-m,", required=True, type=str, choices=[
                        "0", "1", "2a", "2b", "3a", "3b"], help="Noise model(s): \n\t0 -> Gaussian log(d), \n\t1 -> Gaussian d, \n\t3a~ Rician b (approximated by Gaussian), \n\t3b ~ and Rician b")

    args = parser.parse_args()

    args.binning = 2
    args.period = 8
    args.numerical = False
    args.iters = 2001

    with open(args.input/args.config, "r") as f:
        args.yaml = yaml.safe_load(f)

    return args


def setupConstrained(model: str, axdt_op: elsa.axdt.AXDTOperator, absorp_op, ffa: elsa.DataContainer, ffb: elsa.DataContainer, a: elsa.DataContainer, b: elsa.DataContainer, period: int, directions: np.ndarray) -> Tuple[elsa.Functionalf, elsa.SphericalPositivity]:

    loss = setupProblem(model, axdt_op, absorp_op, ffa, ffb, a, b, period)

    Q = elsa.axdt.SphericalHarmonicsTransform(
        axdt_op.getDomainDescriptor(), directions)

    g = elsa.SphericalPositivity(Q)

    if model in {"2a", "2b"}:
        
        h = elsa.IndicatorNonNegativity(absorp_op.getDomainDescriptor())

        return loss, elsa.SeparableSum(h, g)

    else:
        return loss, g


def main():
    args = arguments()
    givenSetup = loadData(args)

    loss, g = setupConstrained(args.model, *givenSetup,
                               args.period, sampling_directions)

    lambdas = np.logspace(-15, -10, num=10)

    solvers = [elsa.APGD(loss, g, l)
               for l in lambdas]

    args.algo = "APGD-fixed"

    writeLoss = set(range(0, args.iters, 10))
    writeImage = set(range(100, args.iters, 100))


    logfile = args.reco_dir / \
        filename("log", args, args.model, args.iters, ".csv")

    with open(logfile, 'w', newline='') as csvfile:

        header = ["dataset", "iteration", "model",
                  "solver", "stepsize", "loss", "time"]
        writer = csv.DictWriter(
            csvfile, header, dialect="excel", extrasaction="raise")

        writer.writeheader()

        for solver, l in zip(solvers, lambdas):

            row = {"dataset": "crossed_sticks", "model": args.model,
                   "solver": args.algo, "stepsize": l}

            start = timer()
            r = trange(args.iters, desc=args.algo+"-"+str(l))

            x = solver.setup()

            for i in r:
                if i in writeImage:
                    save(args, args.model, i, x, "reco")

                if i in writeLoss:

                    row["iteration"] = i
                    row["time"] = timer()-start
                    row["loss"] = loss.evaluate(x)

                    writer.writerow(row)
                    csvfile.flush()

                x = solver.step(x)


if __name__ == '__main__':
    main()
