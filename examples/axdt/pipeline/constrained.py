#!/usr/bin/env python3
import argparse
from pathlib import Path
from typing import Any, List, Optional, Tuple

import numpy as np
import yaml
from tqdm import trange
from utils import *

import pyelsa as elsa

silence()


def setupConstrained(model: str, axdt_op: elsa.axdt.AXDTOperator, absorp_op, ffa: elsa.DataContainer, ffb: elsa.DataContainer, a: elsa.DataContainer, b: elsa.DataContainer, period: int, directions: np.ndarray) -> Tuple[elsa.Functionalf, elsa.SphericalPositivity, float]:

    loss = setupProblem(model, axdt_op, absorp_op, ffa, ffb, a, b, period)

    Q = elsa.axdt.SphericalHarmonicsTransform(
        axdt_op.getDomainDescriptor(), directions)

    g = elsa.SphericalPositivity(Q)

    # Lipschitz constant
    L = 1.1 * elsa.powerIterations(elsa.adjoint(axdt_op) * axdt_op)

    return loss, g, L


def optimizeConstrained(model: str, loss: elsa.Functionalf, constraint: elsa.Functionalf, L: float, args: argparse.Namespace):

    solver = elsa.APGD(loss, constraint, mu=1 / L)

    x = solver.setup()

    r = trange(args.iters, desc=f"Model {model}")

    for i in r:
        if args.save_every is not None and i % args.save_every == 0:
            save(args, model, i, x, "reco")
            save(args, model, i, loss.getGradient(x), "grad")
        x = solver.step(x)

    return x


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='AXDT Reconstruction')

    parser.add_argument("input", type=Path, help="Input directory")
    parser.add_argument("--config", "-c", type=Path,
                        default="dataset_dci.yaml", help="Name of YAML config file")

    parser.add_argument("--reco-dir", "-r", required=True, type=Path,
                        help="Path to store reconstruction at")

    parser.add_argument("--binning", "-b", type=int, default=2,
                        help="Binning factor reducing resolution but also computational cost")
    parser.add_argument("--period", "-p", type=int,
                        default=8, help="Grating periods")
    parser.add_argument("--models", "-m,", required=True, nargs="+", type=str, choices=[
                        "0", "1", "2a", "2b", "3a", "3b"], help="Noise model(s): \n\t0 -> Gaussian log(d), \n\t1 -> Gaussian d, \n\t2a~ Rician b (approximated by Gaussian), \n\t2b ~ and Rician b")

    parser.add_argument("--iters", "-i", type=int, default=10,
                        help="Number of iterations for reconstruction")

    parser.add_argument("--save-every", "-e", type=int,
                        help="Number of iterations between savepoints")

    args = parser.parse_args()
    args.algo = "APGD"
    args.numerical = False

    with open(args.input/args.config, "r") as f:
        args.yaml = yaml.safe_load(f)

    givenSetup = loadData(args)

    for model in args.models:

        bundle = setupConstrained(
            model, *givenSetup, args.period, sampling_directions)

        reconstruction = optimizeConstrained(model, *bundle, args)
        save(args, model, args.iters, reconstruction, "reco")
