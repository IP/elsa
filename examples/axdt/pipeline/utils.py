import argparse
from typing import List, Optional, Tuple, Union

import matplotlib.pyplot as plt
import numpy as np
from scipy.spatial.transform import Rotation as R

import pyelsa as elsa

DC = elsa.DataContainer

# Mapping of model names to fitting titles
titles = {
    "0": "Gaussian log(d)",
    "0'": "Gaussian log(d) (non-uniform variances)",
    "1": "Gaussian d",
    "2a": "Rician b (approximated by Gaussian) (joint)",
    "2b": "Rician b (joint)",
    "3a": "Rician b (approximated by Gaussian) (eta only)",
    "3b": "Rician b (only)"

}

# Ancient sampling directions passed down to us via vecs.csv
# Useful for numerical integration on the sphere (e.g. for numerical weights for an AXDT operator)
sampling_directions = np.array([[0.50748, -0.3062,  0.80543],
                                [-0.3062,  0.80543,  0.50748],
                                [-0.50748,  0.3062,  0.80543],
                                [0.80543,  0.50748, -0.3062],
                                [0.3062,  0.80543, -0.50748],
                                [0.80543, -0.50748,  0.3062],
                                [0.3062, -0.80543,  0.50748],
                                [-0.80543, -0.50748, -0.3062],
                                [-0.3062, -0.80543, -0.50748],
                                [-0.80543,  0.50748,  0.3062],
                                [0.50748,  0.3062, -0.80543],
                                [-0.50748, -0.3062, -0.80543],
                                [0.62636, -0.24353, -0.74052],
                                [-0.24353, -0.74052,  0.62636],
                                [-0.62636,  0.24353, -0.74052],
                                [-0.74052,  0.62636, -0.24353],
                                [0.24353, -0.74052, -0.62636],
                                [-0.74052, -0.62636,  0.24353],
                                [0.24353,  0.74052,  0.62636],
                                [0.74052, -0.62636, -0.24353],
                                [-0.24353,  0.74052, -0.62636],
                                [0.74052,  0.62636,  0.24353],
                                [0.62636,  0.24353,  0.74052],
                                [-0.62636, -0.24353,  0.74052],
                                [-0.28625,  0.95712, -0.044524],
                                [0.95712, -0.044524, -0.28625],
                                [0.28625, -0.95712, -0.044524],
                                [-0.044524, -0.28625,  0.95712],
                                [-0.95712, -0.044524,  0.28625],
                                [-0.044524,  0.28625, -0.95712],
                                [-0.95712,  0.044524, -0.28625],
                                [0.044524,  0.28625,  0.95712],
                                [0.95712,  0.044524,  0.28625],
                                [0.044524, -0.28625, -0.95712],
                                [-0.28625, -0.95712,  0.044524],
                                [0.28625,  0.95712,  0.044524]])

num_sampling_dirs = sampling_directions.shape[0]
sampling_weights = np.repeat(1/num_sampling_dirs, num_sampling_dirs)


def filename(prefix, args, model, i, suffix):
    """
    Create canonical filename storing metadata about the reconstruction
    """
    return f"{prefix}_b{args.binning}_p{args.period}_m{model}_{args.algo}_i{i}{suffix}"


def save(args: argparse.Namespace, model: str, i: int, x: DC, name: str):
    """
    Save a reconstruction to disk
    """
    desc = x.getDataDescriptor()
    if isinstance(desc, elsa.RandomBlocksDescriptor):
        np.save(args.reco_dir / filename(f"{name}_mu", args, model,
                i, ".npy"), np.asarray(x.getBlock(0)))
        np.save(args.reco_dir / filename(f"{name}_eta", args, model,
                i, ".npy"), np.asarray(x.getBlock(1)))
    elif isinstance(desc, elsa.SphericalCoefficientsDescriptor):
        np.save(args.reco_dir / filename(f"{name}_eta", args,
                model, i, ".npy"), np.asarray(x))
    else:
        raise ValueError(f"Unknown descriptor: {desc}")


def set_axes_equal(ax: plt.Axes):
    """Set 3D plot axes to equal scale.

    Make axes of 3D plot have equal scale so that spheres appear as
    spheres and cubes as cubes.  Required since `ax.axis('equal')`
    and `ax.set_aspect('equal')` don't work on 3D.
    Adopted from https://stackoverflow.com/a/63625222
    """
    limits = np.array([
        ax.get_xlim3d(),
        ax.get_ylim3d(),
        ax.get_zlim3d(),
    ])
    origin = np.mean(limits, axis=1)
    radius = 0.5 * np.max(np.abs(limits[:, 1] - limits[:, 0]))

    def _set_axes_radius(ax, origin, radius):
        x, y, z = origin
        ax.set_xlim3d([x - radius, x + radius])
        ax.set_ylim3d([y - radius, y + radius])
        ax.set_zlim3d([z - radius, z + radius])

    _set_axes_radius(ax, origin, radius)


def silence():
    """
    Disable all elsa loggers
    """
    elsa.logger_pyelsa_generators.setLevel(elsa.LogLevel.OFF)
    elsa.logger_pyelsa_line_search.setLevel(elsa.LogLevel.OFF)
    elsa.logger_pyelsa_projectors.setLevel(elsa.LogLevel.OFF)
    elsa.logger_pyelsa_proximal_operators.setLevel(elsa.LogLevel.OFF)
    elsa.logger_pyelsa_functionals.setLevel(elsa.LogLevel.OFF)
    elsa.logger_pyelsa_io.setLevel(elsa.LogLevel.OFF)
    elsa.logger_pyelsa_operators.setLevel(elsa.LogLevel.OFF)
    elsa.logger_pyelsa_projectors_cuda.setLevel(elsa.LogLevel.OFF)
    elsa.logger_pyelsa_solvers.setLevel(elsa.LogLevel.OFF)


def loadStart(args: argparse.Namespace, axdt_op: elsa.axdt.AXDTOperator) -> Optional[DC]:
    """
    Load an initial guess for the reconstruction if provided
    """
    if args.eta0 is None and args.mu0 is None:
        return None

    x = elsa.zeros(axdt_op.getDomainDescriptor())

    if args.eta0 is not None:
        eta0 = np.load(args.eta0)
        if model in ["2a", "2b"]:
            assert (eta0.shape == x.getBlock(1).shape)
            x.getBlock(1).set(DC(eta0))
        else:
            assert (eta0.shape == x.shape)
            x.set(DC(eta0))
    return x


def __binning(data, binning_fac):
    if isinstance(data, DC):
        data = np.asarray(data)
    nproj, nx, ny = data.shape
    bnx, bny = nx // binning_fac, ny // binning_fac
    shape = (nproj, bnx, data.shape[1] // bnx, bny, data.shape[2] // bny)
    return data.reshape(shape).mean(-1).mean(2)


def preprocess(data, binning_fac):
    """Preprocess data by binning and necessary reordering"""
    desc = data.getDataDescriptor()
    data_cp = np.array(data).copy()

    data_cp = __binning(data_cp, binning_fac)
    data_cp = np.transpose(data_cp, (0, 2, 1))

    new_spacing = desc.getSpacingPerDimension() * binning_fac
    new_spacing[-1] = 1
    new_desc = VolumeDescriptor([desc.getNumberOfCoefficientsPerDimension()[0] // binning_fac,
                                 desc.getNumberOfCoefficientsPerDimension()[
        1] // binning_fac,
        desc.getNumberOfCoefficientsPerDimension()[2]], new_spacing)

    return DC(data_cp, new_desc)


def loadData(args: argparse.Namespace) -> Tuple[elsa.axdt.AXDTOperator, elsa.LinearOperator, DC, DC, DC, DC]:
    """Load configuration, load imaging axdt.

    Parameters
    ----------
    config_path : pathlib.Path
        Path to dataset_dci.yml file
    binning_factor : int
        Bin/average data according to binning factor

    Returns
    -------
    axdt_op : LinearOperator
        Operator that describes the AXDT setup
    ffa : DC
        Reference mean intensity of stepping at each position (acquired without object in beam)
    ffb : DC
        Reference mean amplitude of stepping at each position (acquired without object in beam)
    a : DC
        Mean intensity of stepping at each position (acquired with object in beam)
    b : DC
        Mean amplitude of stepping at each position (acquired with object in beam)
    """

    folder = args.input
    binning_factor = args.binning
    config = args.yaml

    # path to _a_ <-- corresponds to absorption
    a_path = folder / config["data"]["a-file"]
    # path to _b_ <-- dark-field can be computed form here
    b_path = folder / config["data"]["b-file"]
    # path to reference/flat field a
    ffa_path = folder / config["data"]["ffa-file"]
    # path to reference/flat field b
    ffb_path = folder / config["data"]["ffb-file"]

    a_neglog = config["data"]["a-neglog-applied"]
    b_neglog = config["data"]["b-neglog-applied"]
    ffa_neglog = config["data"]["ffa-neglog-applied"]
    ffb_neglog = config["data"]["ffb-neglog-applied"]

    assert (not a_neglog and not b_neglog and not ffa_neglog and not ffb_neglog)

    if config["projection"]["type"] == "parallel":
        is_parallel = True
        dst_src_center = 1000000.0  # TODO: Chosen arbitrarily?
        dst_detec_center = 1.0
    else:
        assert (config["projection"]["type"] == "projective")
        is_parallel = False
        dst_src_center = config["projection"]["source-center"]
        dst_detec_center = config["projection"]["detector-center"]

    angle_path = folder / config["projection"]["angles-file"]
    sense_dir = config["projection"]["sensDir"]
    print(f"Sensing direction: {sense_dir}")

    vol_sz = config["reconstruction"]["size"]
    for i in range(len(vol_sz)):
        vol_sz[i] //= binning_factor

    vol_spacing = config["reconstruction"]["spacing"]
    for i in range(len(vol_sz)):
        vol_spacing[i] *= binning_factor

    print(f"Reconstruction volume size   : {vol_sz}")
    print(f"Reconstruction volume spacing: {vol_spacing}")

    sph_degree = int(config["reconstruction"]["sphMaxDegree"])

    print(f"Max degree of spherical harmonics: {sph_degree}")

    vol_desc = elsa.VolumeDescriptor(vol_sz, vol_spacing)

    # Read data
    a = preprocess(elsa.EDF.readf(str(a_path.absolute())), binning_factor)
    b = preprocess(elsa.EDF.readf(str(b_path.absolute())), binning_factor)
    ffa = preprocess(elsa.EDF.readf(str(ffa_path.absolute())), binning_factor)
    ffb = preprocess(elsa.EDF.readf(str(ffb_path.absolute())), binning_factor)

    assert (a.getDataDescriptor() == b.getDataDescriptor())
    assert (ffa.getDataDescriptor() == ffb.getDataDescriptor())

    range_desc = a.getDataDescriptor()

    indexed_angles = np.loadtxt(angle_path, comments="#", delimiter=" ")
    index_expected = np.arange(1, indexed_angles.shape[0] + 1)
    angles = indexed_angles[:, [2, 1, 3]].copy()

    geos = []
    vol_shape = vol_desc.getNumberOfCoefficientsPerDimension()
    range_shape = range_desc.getNumberOfCoefficientsPerDimension()
    for i in range(angles.shape[0]):
        rot_mat = R.from_euler("YZY", angles[i], degrees=True).as_matrix()
        geos.append(elsa.Geometry(dst_src_center, dst_detec_center,
                    vol_shape[:3], range_shape[:2], rot_mat))

    x = a.shape[2]
    y = a.shape[1]
    print(f"Loaded {len(geos)} poses of {x}×{y} pixel detector...")

    sph_desc = elsa.SphericalCoefficientsDescriptor(
        vol_desc, elsa.axdt.Symmetry.Even, sph_degree)

    xgi_desc = elsa.axdt.XGIDetectorDescriptor(range_desc.getNumberOfCoefficientsPerDimension(),
                                               range_desc.getSpacingPerDimension(),
                                               geos, sense_dir, is_parallel)

    projector = elsa.JosephsMethodCUDA(vol_desc, xgi_desc)

    if args.numerical:
        print("Calculating numerical weights...")
        weights = elsa.axdt.numericalWeightingFunction(
            xgi_desc, elsa.axdt.Symmetry.Even, sph_degree, sampling_directions)
    else:
        print("Calculating analytical weights...")
        weights = elsa.axdt.exactWeightingFunction(
            xgi_desc, elsa.axdt.Symmetry.Even, sph_degree)

    axdt_op = elsa.axdt.AXDTOperator(
        sph_desc, xgi_desc, projector, weights)

    shape = range_desc.getNumberOfCoefficientsPerDimension()
    spacing = range_desc.getSpacingPerDimension()
    proj_desc = elsa.PlanarDetectorDescriptor(shape, spacing, geos)
    absorp_op = elsa.JosephsMethodCUDA(vol_desc, proj_desc)

    return axdt_op, absorp_op, ffa, ffb, a, b
