#!/usr/bin/env python3
import argparse
import itertools
from pathlib import Path
from typing import List, Optional, Tuple

import matplotlib.figure
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.widgets import Slider
from utils import *

plt.rcParams['text.usetex'] = True


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='AXDT Projection Visualization')

    parser.add_argument("input", type=Path,
                        help="Dark field data folder")

    parser.add_argument("--output", "-o", type=Path,
                        help="Path to store visualization at")

    parser.add_argument("--interactive", "-i", action="store_true",
                        help="Show interactive plot")

    args = parser.parse_args()

    if args.output is None and not args.interactive:
        print("Need to supply output directory or --interactive!")
        sys.exit(1)

    a_path = args.input / "a.edf"
    b_path = args.input / "b.edf"
    ffa_path = args.input / "ffa.edf"
    ffb_path = args.input / "ffb.edf"

    a = np.asarray(elsa.EDF.readf(str(a_path)))
    b = np.asarray(elsa.EDF.readf(str(b_path)))
    ffa = np.asarray(elsa.EDF.readf(str(ffa_path)))
    ffb = np.asarray(elsa.EDF.readf(str(ffb_path)))

    d = np.log(b * ffa / (a * ffb)) * 1000

    ar = np.log(a/ffa) * 1000
    br = np.log(b/ffb) * 1000

    fig, axes = plt.subplots(ncols=3, nrows=1, sharex=True, sharey=True)

    fig.suptitle(f"AXDT projection visualization")

    axes[0].set_title(r"$\ln{\frac{a}{ffa}}$")
    axes[1].set_title(r"$\ln\frac{b}{ffb}$")
    axes[2].set_title(r"$\ln d = \ln \frac{b}{a} \cdot \frac{ffa}{ffb}$")

    plots = [ar, br, d]

    # Initially plot the slice at z=0
    img = [axes[i].imshow(plots[i][0], cmap="gray") for i in range(len(plots))]

    # Optionally save the plot to disk
    if args.output is not None:
        fig.savefig(args.output)

    if args.interactive:

        numProjections = d.shape[0]

        fig.subplots_adjust(bottom=0.25)
        ax_i = fig.add_axes([0.25, 0.1, 0.65, 0.03])

        allowed_indices = range(numProjections)

        slider = Slider(
            ax_i, "pose", 0, numProjections-1,
            valinit=0, valstep=allowed_indices,
            color="green"
        )

        # Allow the user to interactively change the slice
        def update(i):
            for j in range(len(plots)):
                img[j].set_data(plots[j][i])
                img[j].norm.autoscale(plots[j][i])
            fig.canvas.flush_events()

        slider.on_changed(update)

        plt.show()
    else:
        plt.close(fig)
