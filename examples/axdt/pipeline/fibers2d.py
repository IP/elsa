#!/usr/bin/env python3
import argparse
import sys
from pathlib import Path
from typing import List, Optional, Tuple

import matplotlib.figure
import matplotlib.pyplot as plt
import numpy as np
from fibers3d import direction2color3D
from matplotlib.widgets import Slider
from utils import *

import pyelsa as elsa


def direction2color(direction: np.ndarray) -> np.ndarray:
    """
    Arbitrarily map a 2d direction to a color
    """
    samples = np.array([
        [0.8, 0, 1, 0, 0, 2],
        [0, 0.8, 0, 0, 1, 2],
        [0.3, 0.3, 0, 1, 0, 1.8],
    ])

    def _norm(x):
        # np.linalg.norm, doesn't really work for multi-dim stuff we want here
        return x / (np.sqrt((x ** 2).sum(-1))[..., np.newaxis])

    vn = _norm(direction)

    n = samples.shape[0]
    ndirs = direction.shape[0]

    # run through the samples, accumulate, and normalize afterward
    rgb = np.zeros((ndirs, 3))
    for i in range(n):
        sv = _norm(samples[i, 0:2])
        sc = samples[i, 2:5] / np.max(samples[i, 2:5])
        sf = samples[i, 5]

        # compute angle between sample and query vector
        factor = np.arcsin(np.abs(np.sum(vn * sv, axis=1))) / (np.pi / 2) * sf
        rgb = rgb + factor[..., np.newaxis] * sc

    rgb = rgb / (np.max(rgb, axis=1) + np.finfo(float).eps)[..., np.newaxis]
    return rgb


def visualizeFibers2D(ax: matplotlib.axes.Axes, magnitudes: np.ndarray, indices: np.ndarray, samplingDirections: np.ndarray, attenuation: Optional[np.ndarray], zScore: float, zIndex: int) -> None:
    """
    Visualize the fibers (and optionally attenuation) in a 2D slice of the volume
    Parameters
    ----------
    ax : matplotlib.axes.Axes
        The axis to plot on
    magnitudes : np.ndarray
        The scattering strengths of the fibers
    indices : np.ndarray
        The indices of the directions of the local maxima
    samplingDirections : np.ndarray
        The directions of the fibers
    attenuation : Optional[np.ndarray]
        The attenuation density to plot as background
    zScore : float
        Number of standard deviations to consider for fiber thresholding
    zIndex : int
        The slice to visualize

    """

    if attenuation is not None:
        ax.imshow(attenuation[zIndex, :, :], cmap="gray")

    # Take slice at height iz

    mag = magnitudes[:, zIndex, :, :]
    idx = indices[:, zIndex, :, :]
    threshold = np.mean(magnitudes) + zScore * \
        np.std(magnitudes)[..., np.newaxis]

    idxes = np.where(mag > threshold)
    _, iy, ix = idxes
    dirs = samplingDirections[idx[idxes]]
    col = [direction2color3D(d) for d in dirs]

    # Project onto x-y plane
    dx, dy = (
        dirs[:, :2] * (1.2 - ((2 * mag[idxes]) / np.max(mag)))[..., np.newaxis]).T

    x = np.asarray([ix - dx, ix + dx])
    y = np.asarray([iy - dy, iy + dy])

    [ax.plot(xx, yy, '-', c=c) for xx, yy, c in zip(x.T, y.T, col)]


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='AXDT 2D Fiber Visualization')

    parser.add_argument("--fibers", "-i", required=True, type=Path,
                        help="Dark field signal coefficients to visualize")

    parser.add_argument("--mu", "-m", type=Path,
                        help="Attenuation density as background")

    parser.add_argument("--output", "-o", type=Path,
                        help="Path to store visualization at")

    parser.add_argument("--no-show", action="store_true",
                        help="Don't show the visualization interactively")

    parser.add_argument("--num", "-n", type=int, default=1,
                        help="Number of local maxima/fibers to plot")

    parser.add_argument("--zscore", "-z", type=float, default=0.5,
                        help="Number of standard deviations to consider for fiber thresholding")

    args = parser.parse_args()

    if args.output is None and args.no_show:
        print("Need to supply output directory or omit no-show!")
        sys.exit(1)

    data = np.load(args.fibers)

    # Extract fibers using a C++ helper function
    magnitudes, indices, directions = data["mag"], data["ind"], data["dir"]

    if args.num > magnitudes.shape[0]:
        print(
            f"WARNING: Requested top {args.num} fibers, but only {magnitudes.shape[0]} available in file")
        sys.exit(1)

    magnitudes = magnitudes[:args.num]
    indices = indices[:args.num]

    if args.mu:
        mu = np.load(args.mu)
    else:
        plt.style.use('dark_background')
        mu = None

    fig = plt.figure(figsize=(10, 10))

    ax = fig.add_subplot()
    ax.set_aspect('equal')
    ax.set_xlim(0, magnitudes.shape[1])
    ax.set_ylim(0, magnitudes.shape[2])
    fig.suptitle(f"AXDT fiber visualization - {args.fibers}")

    # Initially plot the slice at z=0
    visualizeFibers2D(ax, magnitudes, indices, directions, mu, args.zscore, 0)

    # Optionally save the plot to disk
    if args.output is not None:
        fig.savefig(args.output)

    if not args.no_show:

        height = magnitudes.shape[1]

        fig.subplots_adjust(bottom=0.25)
        ax_i = fig.add_axes([0.25, 0.1, 0.65, 0.03])

        allowed_indices = range(height)

        slider = Slider(
            ax_i, "z", 0, height-1,
            valinit=0, valstep=allowed_indices,
            color="green"
        )

        # Allow the user to interactively change the slice
        def update(i):
            ax.clear()
            visualizeFibers2D(ax, magnitudes, indices,
                              directions, mu, args.zscore, i)
            ax.set_xlim(0, magnitudes.shape[1])
            ax.set_ylim(0, magnitudes.shape[2])
            fig.canvas.draw_idle()

        slider.on_changed(update)

        plt.show()
    else:
        plt.close(fig)
