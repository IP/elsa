import numpy as np
import pyelsa as elsa
import matplotlib.pyplot as plt

# define Shepp-Logan phantom
shape = np.array([64] * 3)
phantom = elsa.phantoms.modifiedSheppLogan(shape)
vol_desc = phantom.getDataDescriptor()
phantom = np.asarray(phantom)

# Define geometry
nangles = 720
arc = 360
DSD = shape[0] * 100
DSO = shape[0] * 2
angles = np.linspace(0, arc, nangles, endpoint=False)
sino_desc = elsa.CircleTrajectoryGenerator.trajectoryFromAngles(
    angles, vol_desc, DSD, DSO)

# We analytically compute the sinogram
b = elsa.analyticalSheppLogan(vol_desc, sino_desc)

# This is our projection operator
A = elsa.JosephsMethodCUDA(vol_desc, sino_desc)

# Use CGLS to solve min_x 0.5 * ||Ax - b||_2^2
cgls = elsa.CGLS(A, b)
sol1 = cgls.solve(20)

# Use APGD/FISTA to solve min_{x>=0} 0.5 * ||Ax - b||_2^2
L = elsa.powerIterations(elsa.adjoint(A) * A) * 1.1
h = elsa.IndicatorNonNegativity(vol_desc)
apgd = elsa.APGD(A, b, h, mu=1/L)
sol2 = apgd.solve(20)

# And plot it
fig, ax = plt.subplots(1, 3, figsize=(10, 5))
ax[0].imshow(np.array(phantom)[32, :, :], cmap="gray")
ax[1].imshow(np.array(sol1)[32, :, :], cmap="gray")
ax[2].imshow(np.array(sol2)[32, :, :], cmap="gray")
plt.show()
