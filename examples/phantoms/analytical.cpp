#include "DataContainer.h"
#include "DataDescriptor.h"
#include "JosephsMethod.h"
#include "VolumeDescriptor.h"
#include "Phantoms.h"
#include "AnalyticalSinogram.h"
#include "CircleTrajectoryGenerator.h"
#include "SIRT.h"
#include "IO.h"

using namespace elsa;
using namespace elsa::phantoms;

int main(int, char*[])
{

    VolumeDescriptor image{{100, 100}};

    index_t numAngles{180}, arc{360};
    const auto distance = 100.0;
    auto sinoDescriptor = CircleTrajectoryGenerator::createTrajectory(numAngles, image, arc,
                                                                      distance * 100.0f, distance);

    auto analyticalSinogram = analyticalSheppLogan<float>(image, *sinoDescriptor);

    io::write(analyticalSinogram, "analyticalSinogram.pgm");

    JosephsMethod projector{image, *sinoDescriptor};

    auto phantom =
        phantoms::modifiedSheppLogan<real_t>(image.getNumberOfCoefficientsPerDimension());

    auto forwardSinogram = projector.apply(phantom);
    io::write(forwardSinogram, "forwardSinogram.pgm");

    io::write(elsa::cwiseAbs(analyticalSinogram - forwardSinogram), "diff.pgm");

    // solve the reconstruction problem
    SIRT solver(projector, analyticalSinogram);

    index_t noIterations{50};
    Logger::get("Info")->info("Solving reconstruction using {} iterations", noIterations);
    auto reco = solver.solve(noIterations);

    // write the reconstruction out
    io::write(reco, "reconstruction.pgm");

    return 0;
}
