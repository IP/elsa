# This example of the HelixTrajectoryGenerator is a heavily modified version of https://github.com/faebstn96/helix2fan/blob/master/read_data.py

import numpy as np
import tqdm
import os
import pydicom
import struct
from pathlib import Path
import math
import argparse
from pydicom.datadict import DicomDictionary, keyword_dict
from pydicom.tag import Tag

import pyelsa as elsa


def unpack_tag(data, tag):
    return struct.unpack("f", data[tag].value)[0]


def read_projections(folder, indices):
    """Read DICOM-CT-PD projection data following the DICOM-CT-PD User Manual Version 3
    of the TCIA LDCT-and-Projection-data.

    :param folder: full path to projection data folder
    :param indices: slice object that describes the range of helical projections to be loaded
    :return: An array containing the loaded DICOM projections to access header information, the raw projection data
    """
    datasets = []

    # Get the relevant file names.
    file_names = sorted([f for f in os.listdir(folder) if f.endswith(".dcm")])

    if len(file_names) == 0:
        raise ValueError("No DICOM files found in {}".format(folder))

    file_names = file_names[indices]

    raw_projections = None

    for i, file_name in enumerate(tqdm.tqdm(file_names, "Loading projection data")):
        # Read the DICOM projection.
        dataset = pydicom.dcmread(folder / file_name)

        # Get required information.
        rows = dataset.Rows
        cols = dataset.Columns
        # hu_factor = float(dataset[0x70411001].value)  # WaterAttenuationCoefficient, see manual for HU conversion.
        rescale_intercept = dataset.RescaleIntercept
        rescale_slope = dataset.RescaleSlope

        # Load the array as bytes.
        proj_array = np.array(np.frombuffer(dataset.PixelData, "H"), dtype="float32")
        proj_array = proj_array.reshape([cols, rows], order="F")

        # Rescale array according to the TCIA (LDCT-and-Projection-data) DICOM-CT-PD User Manual Version 3.
        proj_array *= rescale_slope
        proj_array += rescale_intercept
        # proj_array /= hu_factor

        # Store results.
        if raw_projections is None:
            # We need to load the first dataset before we know the shape.
            raw_projections = np.empty((len(file_names), cols, rows), dtype="float32")

        raw_projections[i] = proj_array[:, ::-1]
        datasets.append(dataset)

    return datasets, raw_projections


def read_dicom(path_dicom, idx_proj_start, idx_proj_stop):
    """Read DICOM-CT-PD projection data and header information following the DICOM-CT-PD User Manual Version 3
    of the TCIA LDCT-and-Projection-data.

    :param parser: parser containing projection information
    :return: the raw projection data, a parser that contains all relevant DICOM header
    parameters for rebinning and reconstruction
    """

    indices = slice(idx_proj_start, idx_proj_stop)
    data_headers, raw_projections = read_projections(path_dicom, indices)

    # Read geometry information from the DICOM headers following instructions from the
    # TCIA (LDCT-and-Projection-data) DICOM-CT-PD User Manual Version 3.
    angles = np.array([unpack_tag(d, 0x70311001) for d in data_headers])
    angles = np.unwrap(angles)

    rows = struct.unpack("H", data_headers[0][0x70291010].value)[0]
    columns = struct.unpack("H", data_headers[0][0x70291011].value)[0]

    row_width = unpack_tag(data_headers[0], 0x70291006)
    column_width = unpack_tag(data_headers[0], 0x70291002)

    dso = unpack_tag(data_headers[0], 0x70311003)  # DetectorFocalCenterRadialDistance
    dsd = unpack_tag(data_headers[0], 0x70311031)  # ConstantRadialDistance
    ddo = dsd - dso  # ConstantRadialDistance - DetectorFocalCenterRadialDistance

    pitch = data_headers[0][0x00189311].value

    # Calculate z_volume_spacing
    z_volume_spacing = data_headers[0][0x00189311].value * row_width

    detector_width = columns * column_width
    detector_curve_rad = detector_width / dsd

    # Convert angles from radians to degrees because elsa expects degrees
    angles = angles * (180 / np.pi)

    metadata = {
        "angles": angles,
        "rows": rows,
        "row_width": row_width,
        "columns": columns,
        "column_width": column_width,
        "dso": dso,
        "dsd": dsd,
        "ddo": ddo,
        "pitch": pitch,
        "z_volume_spacing": z_volume_spacing,
        "detector_curve_rad": detector_curve_rad,
    }

    return raw_projections, metadata


def create_volume_descriptor(resolution, metadata):
    magnification = metadata["dsd"] / metadata["dso"]
    vol_spacing_x = metadata["column_width"] / (magnification * resolution)
    vol_spacing_y = metadata["row_width"] / (magnification * resolution)
    vol_spacing_z = metadata["z_volume_spacing"]
    vol_size = int(
        math.ceil(np.max([metadata["columns"], metadata["rows"]]) * resolution)
    )

    volume_descriptor = elsa.VolumeDescriptor(
        [vol_size] * 3, [vol_spacing_x, vol_spacing_y, vol_spacing_z]
    )
    return volume_descriptor


def setup_sinogram(raw_projections, metadata, volume_descriptor):
    sino_descriptor = elsa.CurvedHelixTrajectoryGenerator.createTrajectory(
        volume_descriptor,
        metadata["angles"],
        metadata["pitch"],
        metadata["dso"],
        metadata["ddo"],
        metadata["detector_curve_rad"],
        [0, 0],
        [0, 0, 0],
        [metadata["columns"], metadata["rows"]],
        [metadata["column_width"], metadata["row_width"]],
    )

    return elsa.DataContainer(raw_projections, sino_descriptor)


def reconstruct(A, b, niters=70):
    h = elsa.IndicatorNonNegativity(A.getDomainDescriptor())
    L = elsa.powerIterations(elsa.adjoint(A) * A) * 1.1
    solver = elsa.APGD(A, b, h, mu=1 / L)
    return solver.solve(niters)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--dicom-path", type=Path, help="Path to DICOM-CT-PD projection data"
    )
    parser.add_argument(
        "--idx-proj-start",
        default=12000,
        type=int,
        help="Index of the first projection",
    )
    parser.add_argument(
        "--idx-proj-stop", default=16000, type=int, help="Index of the last projection"
    )
    parser.add_argument(
        "--resolution", default=1, type=float, help="Resolution scaling of volume"
    )
    parser.add_argument("--niters", default=70, type=int, help="Number of iterations")

    args = parser.parse_args()

    raw_projections, metadata = read_dicom(
        args.dicom_path, args.idx_proj_start, args.idx_proj_stop
    )

    volume_descriptor = create_volume_descriptor(args.resolution, metadata)
    sinogram = setup_sinogram(raw_projections, metadata, volume_descriptor)
    sino_descriptor = sinogram.getDataDescriptor()

    projector = elsa.SiddonsMethodCUDA(volume_descriptor, sino_descriptor)

    reconstruction = reconstruct(projector, sinogram, niters=args.niters)

    elsa.visualize_reconstruction(reconstruction)
