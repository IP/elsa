"""This is a reconstruction of the gel phantom provided by the Finnish Inverse
Problems Society (FIPS).

The DOI of the data is 10.5281/zenodo.7876521. It can be found at
https://zenodo.org/record/7876521. To run the script, download
the zip files and extract them in a common folder. Then pass the path as
an argument to the script

A description taken from the FIPS website (https://www.fips.fi/dataset.php#gel)
describing the dataset:
> This is an open-access dataset of tomographic X-ray data of a dynamic
> agarose-gel phantom perfused with a liquid contrast agent. The dataset
> consists of 17 consecutive X-ray measurements of a 2D slice of the gel
> phantom. This data is provided in two resolutions and stored in a special
> CT-data structure containing the metadata of the measurement setup.

In this example, the 17 different time steps are combined into one combined
2D+t reconstruction. T

the constrained optimization problem with the least squares
data term with a non-negativity constraint is solved using APGD/FISTA.
"""
from pathlib import Path
import argparse
import math

import matplotlib.pyplot as plt
import mat73
import numpy as np
import pyelsa as elsa
import itertools
import tifffile
import imageio.v3 as imageio


def define_sino_descriptor(projections, params, step=1, resolution=1.0, binning=2):
    # For now assume they have the exact same parameters
    angles = params[0]["angles"][::step]
    sourceToDetector = params[0]["distanceSourceDetector"]
    sourceToOrigin = float(params[0]["distanceSourceOrigin"])
    originToDetector = float(sourceToDetector - sourceToOrigin)

    detectorSize = projections[0].shape[:-1]
    detectorSpacing = [params[0]["effectivePixelSize"] * binning]

    # Compute magnification
    magnification = sourceToDetector / sourceToOrigin

    # Determine good size and spacing for volume
    vol_spacing = detectorSpacing[0] / (magnification * resolution)
    size = int(math.ceil(np.max(detectorSize) * resolution))

    single_vol_desc = elsa.VolumeDescriptor([size] * 2, [vol_spacing] * 2)

    single_descriptor = elsa.CircleTrajectoryGenerator.trajectoryFromAngles(
        angles,
        single_vol_desc,
        sourceToOrigin,
        originToDetector,
        [0],
        [0, 0],  # Offset of origin
        detectorSize,
        detectorSpacing,
    )
    sino_descriptor = elsa.IdenticalBlocksDescriptor(
        len(params), single_descriptor)
    volume_descriptor = elsa.IdenticalBlocksDescriptor(
        len(params), single_vol_desc)

    projections = np.transpose(projections, (1, 2, 0))
    projections = np.flip(projections, axis=1)
    projections = np.roll(projections, -90, axis=1)
    projections = projections[:, ::step, :]

    return elsa.DataContainer(projections, sino_descriptor), volume_descriptor


def reconstruct(A, b, niters=50):
    """Just a basic reconstruction which looks nice"""
    g = elsa.IndicatorNonNegativity(A.getDomainDescriptor())
    solver = elsa.APGD(A, b, g)
    return np.array(solver.solve(niters))


def reconstruct_ladmm_temporaltv(A, b, tau=None, sigma=None, tvreg=None, niters=70):
    """Just a basic reconstruction which looks nice"""
    grad = elsa.FiniteDifferences(
        A.getDomainDescriptor(), [False, False, True])

    # Define Proximal Operators
    proxg1 = elsa.ProximalL2Squared(b)
    proxg2 = elsa.ProximalL1(tvreg)

    # Combine them to one
    proxg = elsa.CombinedProximal(proxg1, proxg2)

    # Setup stacked operator containing A, and Gradient
    K = elsa.BlockLinearOperator(
        [A, grad],
        elsa.BlockLinearOperator.BlockType.ROW,
    )

    # Use box constraint as proximal for f
    proxf = elsa.ProximalBoxConstraint(0)

    admm = elsa.LinearizedADMM(K, proxf, proxg, sigma, tau)
    return np.array(admm.solve(niters))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Reconstruction of the open-access dataset of agarose gel phantoms."
    )

    parser.add_argument("path", type=Path, help="Path to the matlab files")
    parser.add_argument(
        "-i",
        "--iters",
        default=70,
        type=int,
        help="Number of iterations for the reconstruction",
    )
    parser.add_argument(
        "-b",
        "--binning",
        default=2,
        type=int,
        help="Binning of the sinogram (must be 2 or 4)",
    )
    parser.add_argument(
        "--sigma",
        default=2.0,
        type=float,
        help="Sigma value for ADMM",
    )
    parser.add_argument(
        "--tau",
        default=0.07819242060015812,
        type=float,
        help="Tau value for ADMM",
    )
    parser.add_argument(
        "--tvreg",
        default=0.075,
        type=float,
        help="TV regularization parameter",
    )
    parser.add_argument(
        "--resolution",
        default=1.0,
        type=float,
        help="Change the resolution of the reconstruction (default 1.0)",
    )
    parser.add_argument(
        "--step",
        default=1,
        type=int,
        help="How many angles to skip",
    )
    parser.add_argument(
        "--timestep",
        default=4,
        type=int,
        help="Select timestep to show",
    )

    args = parser.parse_args()

    if args.binning not in (2, 4):
        raise ValueError(f"Binnng must be 2 or 4 (is {args.binning})")

    phantom = f"GelPhantomData_b{args.binning}"
    mat = mat73.loadmat(f"{args.path / phantom}.mat")
    params = mat[phantom]["parameters"]
    sinogram = mat[phantom]["sinogram"]
    sinogram = np.transpose(sinogram, (0, 2, 1))

    binning = args.binning
    iters = args.iters

    # Define the sinogram for the full angular resolution
    sino_full, vol_desc_full = define_sino_descriptor(
        sinogram, params, binning=binning, resolution=args.resolution, step=1
    )
    sino_desc_full = sino_full.getDataDescriptor()

    # Set the forward and backward projector
    A_full = elsa.Diagonal(
        vol_desc_full, sino_desc_full,
        [elsa.JosephsMethodCUDA(vol_desc_full.getDescriptorOfBlock(i), sino_desc_full.getDescriptorOfBlock(i))
         for i in range(0, sino_desc_full.getNumberOfBlocks())],
    )

    # define the sinogram for the reduced angular resolution
    step = args.step
    sino_sparse, vol_desc_sparse = define_sino_descriptor(
        sinogram, params, binning=binning, resolution=args.resolution, step=step
    )
    sino_desc_sparse = sino_sparse.getDataDescriptor()

    # Set the forward and backward projector
    A_sparse = elsa.Diagonal(
        vol_desc_sparse, sino_desc_sparse,
        [elsa.JosephsMethodCUDA(vol_desc_sparse.getDescriptorOfBlock(i), sino_desc_sparse.getDescriptorOfBlock(i))
         for i in range(0, sino_desc_sparse.getNumberOfBlocks())],
    )

    recos_gt = reconstruct(A_sparse, sino_sparse, niters=60)

    recos_tv = reconstruct_ladmm_temporaltv(
        A_sparse, sino_sparse, sigma=args.sigma, tau=args.tau, tvreg=args.tvreg, niters=iters)

    from mpl_toolkits.axes_grid1 import make_axes_locatable

    def add_colorbar(fig, ax, im):
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="5%", pad=0.05)
        fig.colorbar(im, cax=cax, orientation="vertical")

    def plot_row(fig, axis, recos):
        im = None
        tf = args.timestep
        ax = axis
        img = recos[:, :, tf]
        im = ax.imshow(img, cmap="inferno")
        # Hide ticks
        ax.axes.get_xaxis().set_visible(False)
        ax.axes.get_yaxis().set_visible(False)

    fig, axes = plt.subplots(nrows=1, ncols=2)
    for ax, rec, title in zip(axes.flat, [recos_gt, recos_tv], ["Ground truth", "Sparse Reco with TV"]):
        plot_row(fig, ax, rec)

    plt.show()
