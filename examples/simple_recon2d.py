# [simplerecon header begin]
import numpy as np
import matplotlib.pyplot as plt

import pyelsa as elsa
# [simplerecon header end]

# [simplerecon phantom begin]
size = np.array([128] * 2)
phantom = elsa.phantoms.modifiedSheppLogan(size)
# [simplerecon phantom end]

# [simplerecon trajectory begin]
numAngles = 720
arc = 360
DSD = size[0] * 100
DSO = size[0] * 2
angles = np.linspace(0, arc, numAngles, endpoint=False)
sinoDescriptor = elsa.CircleTrajectoryGenerator.trajectoryFromAngles(
    angles, phantom.getDataDescriptor(), DSD, DSO)
# [simplerecon trajectory end]

# [simplerecon sino begin]
projector = elsa.JosephsMethod(phantom.getDataDescriptor(), sinoDescriptor)
sinogram = projector.apply(phantom)
# [simplerecon sino end]

# [simplerecon reco begin]
h = elsa.IndicatorNonNegativity(phantom.getDataDescriptor())
solver = elsa.APGD(projector, sinogram, h)
reconstruction = solver.solve(50)
# [simplerecon reco end]

# [simplerecon show begin]
plt.imshow(np.array(reconstruction), cmap="gray")
plt.colorbar()
plt.show()
# [simplerecon show end]
