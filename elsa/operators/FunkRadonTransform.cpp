#include "FunkRadonTransform.h"
#include "Math.hpp"
#include "DataContainer.h"
#include "SphericalCoefficientsDescriptor.h"
#include "TypeCasts.hpp"
#include "elsaDefines.h"
#include <algorithm>

namespace elsa::axdt
{
    /**
     * @brief Helper method for generating the matrix that represents the Funk-Radon transform
     * the spherical harmonics basis. Note that this will be zero for Symmetry::odd
     * cf. https://en.wikipedia.org/wiki/Funk_transform
     *
     * @return Diagonal matrix representing the transformation in the spherical harmonics basis
     */
    template <typename data_t>
    Eigen::DiagonalMatrix<data_t, Eigen::Dynamic> funkRadonMatrix(const Symmetry symmetry,
                                                                  const index_t degree)
    {
        const auto coeffCount = SphericalCoefficientsDescriptor::coefficientCount(symmetry, degree);

        Vector_t<data_t> diagonal = Vector_t<data_t>::Zero(coeffCount);

        index_t l = 0;
        index_t step = (symmetry == Symmetry::regular) ? 1 : 2;

        for (index_t i = 0; i < coeffCount; l += step) {
            for (index_t m = -l; m <= l; m++, i++) {
                diagonal[i] = legendreAtZero<data_t>(l);
            }
        }

        return diagonal.asDiagonal();
    }

    template <typename data_t>
    FunkRadonTransform<data_t>::FunkRadonTransform(
        const SphericalCoefficientsDescriptor& descriptor)
        : LinearOperator<data_t>(descriptor, descriptor)
    {
    }

    template <typename data_t>
    void FunkRadonTransform<data_t>::applyImpl(const DataContainer<data_t>& x,
                                               DataContainer<data_t>& Ax) const
    {
        assert(x.getDataDescriptor() == this->getDomainDescriptor());
        assert(Ax.getDataDescriptor() == this->getRangeDescriptor());

        const auto& etaDesc =
            downcast<const SphericalCoefficientsDescriptor>(x.getDataDescriptor());

        const auto funkRadon = funkRadonMatrix<data_t>(etaDesc.symmetry, etaDesc.degree);

        // We are just a BlockLinearOperator of Scalings!
        for (index_t i = 0; i < etaDesc.getNumberOfBlocks(); i++) {
            Ax.getBlock(i) = funkRadon.diagonal()[i] * x.getBlock(i);
        }
    }

    template <typename data_t>
    void FunkRadonTransform<data_t>::applyAdjointImpl(const DataContainer<data_t>& y,
                                                      DataContainer<data_t>& Aty) const
    {
        this->apply(y, Aty);
    }

    template <typename data_t>
    FunkRadonTransform<data_t>* FunkRadonTransform<data_t>::cloneImpl() const
    {
        return new FunkRadonTransform(
            downcast<const SphericalCoefficientsDescriptor>(this->getDomainDescriptor()));
    }

    template <typename data_t>
    bool FunkRadonTransform<data_t>::isEqual(const LinearOperator<data_t>& other) const
    {
        if (!LinearOperator<data_t>::isEqual(other))
            return false;

        return is<FunkRadonTransform>(other);
    }

    template class FunkRadonTransform<float>;
    template class FunkRadonTransform<double>;

} // namespace elsa::axdt