#include "DataContainer.h"
#include "IdenticalBlocksDescriptor.h"
#include "Math.hpp"
#include "SphericalCoefficientsDescriptor.h"
#include "TypeCasts.hpp"
#include "VolumeDescriptor.h"
#include "elsaDefines.h"
#include "SphericalHarmonicsTransform.h"

namespace elsa::axdt
{

    template <typename data_t>
    Matrix_t<data_t> evalSphericalHarmonics(const Symmetry symmetry, const index_t maxL,
                                            const DirVecList<data_t>& dirs)
    {
        size_t directions = dirs.size();
        Matrix_t<data_t> sphericalHarmonicsBasis{
            directions, SphericalCoefficientsDescriptor::coefficientCount(symmetry, maxL)};

        data_t normFac = sqrt(as<data_t>(4) * pi_t / dirs.size());

#pragma omp parallel for
        for (index_t i = 0; i < asSigned(directions); ++i) {
            index_t j = 0;

            auto dir = dirs[asUnsigned(i)];

            // theta: atan2 returns the elevation, so to get theta = the inclination, we need:
            // inclination = pi/2-elevation azimuth = phi
            auto theta = pi<data_t> / 2 + atan2(dir[2], hypot(dir[0], dir[1]));
            auto phi = atan2(dir[1], dir[0]);
            auto sh_dir = SH_basis_real<data_t>(maxL, theta, phi);

            for (int l = 0; l <= maxL; ++l) {
                for (int m = -l; m <= l; ++m) {
                    if (symmetry == Symmetry::even && (l % 2) != 0) {
                        continue;
                    }

                    sphericalHarmonicsBasis(i, j) = normFac * sh_dir(l * l + l + m);
                    j++;
                }
            }
        }

        return sphericalHarmonicsBasis;
    }

    template <typename data_t>
    SphericalHarmonicsTransform<data_t>::SphericalHarmonicsTransform(
        const SphericalCoefficientsDescriptor& domainDescriptor,
        const DirVecList<data_t>& samplingDirections)
        : LinearOperator<data_t>{domainDescriptor,
                                 IdenticalBlocksDescriptor{
                                     asSigned(samplingDirections.size()),
                                     domainDescriptor.getDescriptorOfBlock(0)}},
          _samplingDirections(samplingDirections),
          _basis{evalSphericalHarmonics<data_t>(domainDescriptor.symmetry, domainDescriptor.degree,
                                                samplingDirections)}
    {
    }

    template <typename data_t>
    SphericalHarmonicsTransform<data_t>::SphericalHarmonicsTransform(
        const SphericalHarmonicsTransform& other)
        : LinearOperator<data_t>{other},
          _samplingDirections(other._samplingDirections),
          _basis(other._basis)
    {
    }

    template <typename data_t>
    SphericalHarmonicsTransform<data_t>* SphericalHarmonicsTransform<data_t>::cloneImpl() const
    {
        return new SphericalHarmonicsTransform<data_t>(*this);
    }

    template <typename data_t>
    bool SphericalHarmonicsTransform<data_t>::isEqual(const LinearOperator<data_t>& other) const
    {
        if (!LinearOperator<data_t>::isEqual(other))
            return false;

        if (!is<SphericalHarmonicsTransform<data_t>>(other))
            return false;

        const auto& otherSFT = downcast<const SphericalHarmonicsTransform<data_t>>(other);

        return _samplingDirections == otherSFT._samplingDirections;
    }

    template <typename data_t>
    void SphericalHarmonicsTransform<data_t>::applyImpl(const DataContainer<data_t>& x,
                                                        DataContainer<data_t>& Ax) const
    {
        assert(x.getDataDescriptor() == this->getDomainDescriptor());
        assert(Ax.getDataDescriptor() == this->getRangeDescriptor());

        const auto& sphDesc =
            downcast<const SphericalCoefficientsDescriptor>(this->getDomainDescriptor());

        const auto numCoeffs = sphDesc.getNumberOfBlocks();
        const index_t numDirs = _samplingDirections.size();
        const auto volSize = x.getSize() / x.getNumberOfBlocks();

        // Eigen is column-major by default
        Eigen::Map<const Matrix_t<data_t>> sphericalCoefficients{
            thrust::raw_pointer_cast(x.storage().data()), volSize, numCoeffs};

        Eigen::Map<Matrix_t<data_t>> sampledDirections{
            thrust::raw_pointer_cast(Ax.storage().data()), volSize, numDirs};

        // Hope that Eigen can cope with these huge matrices...
        // (volSize×dirs) = (volSize×coeffs) * (coeffs×dirs)
        sampledDirections = sphericalCoefficients * _basis.transpose();
    }

    template <typename data_t>
    void SphericalHarmonicsTransform<data_t>::applyAdjointImpl(const DataContainer<data_t>& y,
                                                               DataContainer<data_t>& Aty) const
    {
        assert(y.getDataDescriptor() == this->getRangeDescriptor());
        assert(Aty.getDataDescriptor() == this->getDomainDescriptor());

        const auto& sphDesc =
            downcast<const SphericalCoefficientsDescriptor>(this->getDomainDescriptor());

        const auto numCoeffs = sphDesc.getNumberOfBlocks();
        const index_t numDirs = _samplingDirections.size();
        const auto volSize = y.getSize() / y.getNumberOfBlocks();

        // Eigen is column-major by default
        Eigen::Map<const Matrix_t<data_t>> sampledDirections{
            thrust::raw_pointer_cast(y.storage().data()), volSize, numDirs};

        Eigen::Map<Matrix_t<data_t>> sphericalCoefficients{
            thrust::raw_pointer_cast(Aty.storage().data()), volSize, numCoeffs};

        // This approximates the scalar product on S2 and hence computes the coefficients in the
        // spherical harmonics basis.
        // (volSize×coeffs) = (volSize×dirs) * (dirs×coeffs)
        sphericalCoefficients = sampledDirections * _basis;
    }

    template Matrix_t<float> evalSphericalHarmonics<float>(const Symmetry, const index_t,
                                                           const DirVecList<float>&);

    template Matrix_t<double> evalSphericalHarmonics<double>(const Symmetry, const index_t,
                                                             const DirVecList<double>&);

    template class SphericalHarmonicsTransform<float>;
    template class SphericalHarmonicsTransform<double>;

} // namespace elsa::axdt
