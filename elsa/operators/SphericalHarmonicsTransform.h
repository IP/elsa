#pragma once

#include "SphericalCoefficientsDescriptor.h"
#include "LinearOperator.h"
#include "elsaDefines.h"

namespace elsa::axdt
{

    /**
     * @brief Evaluates the spherical harmonics up to a given order for a list of directions.
     *
     * @tparam data_t
     * @param symmetry
     * @param maxL
     * @param dirs
     * @return Matrix_t<data_t>
     */
    template <typename data_t>
    Matrix_t<data_t> evalSphericalHarmonics(const Symmetry symmetry, const index_t maxL,
                                            const DirVecList<data_t>& dirs);

    /**
     * @brief Linear Operator representing the basis change from the spherical harmonics basis to
     * function values on a list of sampling directions. The domain is therefore to be described by
     * a SphericalCoefficientsDescriptor, while the range has a shape encoded by an
     * IdenticalBlocksDescriptor with the underlying volume repeated for each direction.
     *
     * @tparam data_t
     * @author Cederik Höfs
     */
    template <typename data_t>
    class SphericalHarmonicsTransform : public LinearOperator<data_t>
    {
    public:
        /**
         * @brief Construct a new Spherical Function Transform object
         *
         * @param domainDescriptor
         * @param samplingDirections
         */
        SphericalHarmonicsTransform(const SphericalCoefficientsDescriptor& domainDescriptor,
                                    const DirVecList<data_t>& samplingDirections);

        ~SphericalHarmonicsTransform() override = default;

    protected:
        /// protected copy constructor; used for cloning
        SphericalHarmonicsTransform(const SphericalHarmonicsTransform& other);

        /// implement the polymorphic clone operation
        SphericalHarmonicsTransform<data_t>* cloneImpl() const override;

        /// implement the polymorphic comparison operation
        bool isEqual(const LinearOperator<data_t>& other) const override;

        /// apply the AXDT operator
        void applyImpl(const DataContainer<data_t>& x, DataContainer<data_t>& Ax) const override;

        /// apply the adjoint of the AXDT operator
        void applyAdjointImpl(const DataContainer<data_t>& y,
                              DataContainer<data_t>& Aty) const override;

    private:
        const DirVecList<data_t> _samplingDirections;

        // shape volSize×#coeffs
        const Matrix_t<data_t> _basis;
    };

} // namespace elsa::axdt
