#pragma once

#include "LineSearchMethod.h"

namespace elsa
{
    /**
     * @brief Armijo Condition (Backtracking)
     *
     * ArmijoCondition is a line search method attempting to find a step size
     * @f$\alpha@f$ that satisfies the sufficient decrease condition (also known as the Armijo
     * Condition):
     *
     * @f[
     * f(x_i+\alpha d_i) \le f(x_i) + c \alpha \nabla f_i^T d_i
     * @f]
     *
     * where @f$f: \mathbb{R}^n \to \mathbb{R}@f$ is differentiable,
     * @f$\nabla f_i: \mathbb{R}^n is the gradient of f at x_i@f$,
     * @f$ d_i: \mathbb{R}^n is the search direction @f$, and
     * @f$ c: is a constant @f$.
     *
     * References:
     * - See Wright and Nocedal, ‘Numerical Optimization’, 2nd Edition, 2006, pp. 33-37.
     *
     * @author
     * - Said Alghabra - initial code
     *
     * @tparam data_t data type for the domain and range of the problem, defaulting to real_t
     */
    template <typename data_t = real_t>
    class ArmijoCondition : public LineSearchMethod<data_t>
    {
    public:
        ArmijoCondition(const Functional<data_t>& problem, data_t amax = 2, data_t c = 0.1,
                        data_t rho = 0.5, index_t max_iterations = 10);
        ~ArmijoCondition() override = default;

        /// make copy constructor deletion explicit
        ArmijoCondition(const ArmijoCondition<data_t>&) = delete;

        data_t solve(DataContainer<data_t> xi, DataContainer<data_t> di) override;

        /// implement the polymorphic comparison operation
        bool isEqual(const LineSearchMethod<data_t>& other) const override;

    private:
        // initial guess and largest allowed step size
        data_t _amax;

        // parameter affecting the sufficient decrease condition
        data_t _c;

        // reduction factor for the next step size guess
        data_t _rho;

        /// implement the polymorphic clone operation
        ArmijoCondition<data_t>* cloneImpl() const override;
    };
} // namespace elsa
