#include "SphericalCoefficientsDescriptor.h"
#include "DataDescriptor.h"
#include "IdenticalBlocksDescriptor.h"
#include "TypeCasts.hpp"

namespace elsa
{
    SphericalCoefficientsDescriptor::SphericalCoefficientsDescriptor(
        const DataDescriptor& blockDescriptor, const Symmetry symmetry, const index_t degree)
        : IdenticalBlocksDescriptor{coefficientCount(symmetry, degree), blockDescriptor},
          degree{degree},
          symmetry{symmetry}
    {
    }

    SphericalCoefficientsDescriptor* SphericalCoefficientsDescriptor::cloneImpl() const
    {
        return new SphericalCoefficientsDescriptor{*_blockDescriptor, symmetry, degree};
    }

    bool SphericalCoefficientsDescriptor::isEqual(const DataDescriptor& other) const
    {
        if (!IdenticalBlocksDescriptor::isEqual(other))
            return false;

        // static cast as type checked in base comparison
        const auto& otherBlock = downcast<SphericalCoefficientsDescriptor>(other);
        return degree == otherBlock.degree && symmetry == otherBlock.symmetry;
    }

} // namespace elsa
