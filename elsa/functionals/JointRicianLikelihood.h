#pragma once

#include "AXDTOperator.h"
#include "Functional.h"
#include "RandomBlocksDescriptor.h"

namespace elsa
{
    /**
     * @brief Implement the loss functions presented by Schilling et al (see ref below),
     * which implement the log-likelihood functions for AXDT reconstruction, which
     * follow the Rician distribution.
     *
     * We assume the measurements follow the following formula:
     * \[
     * s_{n,j} = a_j + b_j * \cos(\frac{2 \pi n}{N} - \phi_j),
     * \]
     * This function assumes that \f$ A_j \sim \mathcal{N}(a_j, \frac{a_j}{N})\$, and \f$
     * \frac{1}{2} B_j \sim \mathcal{R} \left( \frac{b_j}{2}, \sqrt{\frac{a_j}{2N}} \right) \f$.
     * Here, capital letters represent the random variables. This formula is an
     * accurate assumption for the measurement of AXDT.
     *
     * Schilling derived the proper log likelihood functionals for the above assumptions.
     * They also present two functionals, one where the Rician is approximated
     * by a Gaussian, which holds under certain assumptions. For the exact formulation,
     * derivative and Hessian, please check the references below.
     *
     * References:
     * - Statistical Models for Anisotropic X-Ray Dark-field Tomography, Schilling et al (2017)
     *
     */
    template <typename data_t = real_t>
    class RicianLoss : public Functional<data_t>
    {
    public:
        /**
         * @brief Construct a Joint Rician Loss
         * the domain consists of the cartesian product of the attenuation data and the spherical
         * harmonics coefficients
         *
         * @param[in] ffa the flat-field measured value of RV a
         * @param[in] ffb the flat-field measured value of RV b
         * @param[in] a the measured value of RV a (with the measured sample in-place)
         * @param[in] b the measured value of RV b (with the measured sample in-place)
         * @param[in] absorp_op operator modeling the projection of the regular absorption signal
         * @param[in] axdt_op operator modeling the projection of the dark-field signal
         * @param[in] N total grating-stepping steps
         * @param[in] approximate if true, use a Gaussian distribution to approximate the Racian
         */
        RicianLoss(const DataContainer<data_t>& ffa, const DataContainer<data_t>& ffb,
                   const DataContainer<data_t>& a, const DataContainer<data_t>& b,
                   const LinearOperator<data_t>& absorp_op, const LinearOperator<data_t>& axdt_op,
                   index_t N, bool approximate);

        /// make copy constructor deletion explicit
        RicianLoss(const RicianLoss<data_t>&) = delete;

        /// default destructor
        ~RicianLoss() override = default;

        bool isDifferentiable() const override;

    protected:
        /// the evaluation of this functional
        data_t evaluateImpl(const DataContainer<data_t>& Rx) const override;

        /// the computation of the gradient
        void getGradientImpl(const DataContainer<data_t>& Rx,
                             DataContainer<data_t>& out) const override;

        /// the computation of the Hessian
        LinearOperator<data_t> getHessianImpl(const DataContainer<data_t>& Rx) const override;

        /// implement the polymorphic clone operation
        RicianLoss<data_t>* cloneImpl() const override;

        /// implement the polymorphic comparison operation
        bool isEqual(const Functional<data_t>& other) const override;

    private:
        /// the flat-field measured value of RV a
        DataContainer<data_t> ffa_;

        /// the flat-field measured value of RV b
        DataContainer<data_t> ffb_;

        /// the measured value of RV a
        DataContainer<data_t> a_tilde_;

        /// the measured value of RV b
        DataContainer<data_t> b_tilde_;

        /// operator modeling the projection of the regular absorption signal
        std::unique_ptr<LinearOperator<data_t>> absorp_op_;

        /// operator modeling the projection of the dark-field signal
        std::unique_ptr<LinearOperator<data_t>> axdt_op_;

        /// store the frequently used value alpha (= ffb / ffa)
        DataContainer<data_t> alpha_;

        /// cache the logarithm to save unnecessary computations
        DataContainer<data_t> log_ffa_;

        /// total grating-stepping steps
        data_t N_;

        /// bool to indicate whether to use a Gaussian distribution to approximate the Racian
        bool approximate_;
    };

} // namespace elsa
