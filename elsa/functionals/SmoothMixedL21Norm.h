#pragma once

#include "Functional.h"

namespace elsa
{
    /**
     * @brief Class representing the smooth mixed L12 functional.
     *
     * General p,q norm definition:
     * @f[
     * \|A\|_{p, q} = \left( \sum_{j=1}^n \left( \sum_{i=1}^m \left| a_{i j} \right|^p
     * \right)^{\frac{q}{p}} \right)^{\frac{1}{q}}
     * @f]
     *
     * The smooth mixed L21 functional evaluates to
     * @f[
     * \|A\|_{2,1} = \left( \sum_{j=1}^n \left( \sum_{i=1}^m \left| a_{ij} \right|^2 +
     * e^2\right)^{\frac{1}{2}} \right)
     * @f]
     *
     */
    template <typename data_t = real_t>
    class SmoothMixedL21Norm : public Functional<data_t>
    {
    public:
        explicit SmoothMixedL21Norm(const DataDescriptor& domainDescriptor, data_t epsilon);

        SmoothMixedL21Norm(const SmoothMixedL21Norm<data_t>&) = delete;

        ~SmoothMixedL21Norm() override = default;

    protected:
        data_t evaluateImpl(const DataContainer<data_t>& Rx) const override;

        void getGradientImpl(const DataContainer<data_t>& Rx,
                             DataContainer<data_t>& out) const override;

        LinearOperator<data_t> getHessianImpl(const DataContainer<data_t>& Rx) const override;

        SmoothMixedL21Norm<data_t>* cloneImpl() const override;

        bool isEqual(const Functional<data_t>& other) const override;

    private:
        data_t epsilon;
    };

} // namespace elsa
