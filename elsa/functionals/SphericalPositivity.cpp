#include "IndicatorFunctionals.h"
#include "DataDescriptor.h"
#include "DataContainer.h"
#include "Error.h"
#include "Functional.h"
#include "SphericalCoefficientsDescriptor.h"
#include "SphericalHarmonicsTransform.h"
#include "SphericalPositivity.h"
#include "elsaDefines.h"
#include <limits>
#include <thrust/logical.h>

namespace elsa
{

    template <class data_t>
    SphericalPositivity<data_t>::SphericalPositivity(
        const SphericalCoefficientsDescriptor& domaindescriptor,
        const axdt::DirVecList<data_t>& directions)
        : SphericalPositivity{
            axdt::SphericalHarmonicsTransform<data_t>{domaindescriptor, directions}}
    {
    }

    template <class data_t>
    SphericalPositivity<data_t>::SphericalPositivity(
        const axdt::SphericalHarmonicsTransform<data_t>& Q)
        : Functional<data_t>{Q.getDomainDescriptor()},
          Q{downcast<axdt::SphericalHarmonicsTransform<data_t>>(Q.clone())}
    {
    }

    template <class data_t>
    data_t SphericalPositivity<data_t>::evaluateImpl(const DataContainer<data_t>& Rx) const
    {
        constexpr auto infinity = std::numeric_limits<data_t>::infinity();

        auto evaluated = Q->apply(Rx);

        auto clipped = elsa::maximum(evaluated, 0);

        return (clipped - evaluated).sum() > 0 ? infinity : 0;
    }

    template <class data_t>
    data_t SphericalPositivity<data_t>::convexConjugate(const DataContainer<data_t>& x) const
    {
        throw NotImplementedError("IndicatorNonNegativity: Not implemented (yet)");
    }

    template <class data_t>
    void SphericalPositivity<data_t>::getGradientImpl(const DataContainer<data_t>&,
                                                      DataContainer<data_t>&) const
    {
        throw NotImplementedError("IndicatorNonNegativity: Not differentiable");
    }

    template <class data_t>
    LinearOperator<data_t>
        SphericalPositivity<data_t>::getHessianImpl(const DataContainer<data_t>&) const
    {
        throw NotImplementedError("IndicatorNonNegativity: Not differentiable");
    }

    template <class data_t>
    DataContainer<data_t> SphericalPositivity<data_t>::proximal(const DataContainer<data_t>& v,
                                                                data_t /*t*/) const
    {
        DataContainer<data_t> out(v.getDataDescriptor());
        proximal(v, 1.0, out);
        return out;
    }

    template <class data_t>
    void SphericalPositivity<data_t>::proximal(const DataContainer<data_t>& v, data_t /* t */,
                                               DataContainer<data_t>& out) const
    {
        // cf. Daniel O’Connor,  D., and Vandenberghe, L., “Primal-Dual Decomposition by Operator
        // Splitting and Applications to Image Deblurring”, SIAM J.Imaging Sciences, vol.7,
        // pp.1724–1754. 2014.

        // For finite degrees of spherical harmonics and number of sampling directions, we only have
        // Q^T Q = I, Q Q^T = I approximately, the former probably more accurately than the latter.
        // After clipping we might well fall outside the range of Q...

        // TODO: This is _very_ memory intensive for a large number of directions... Should do this
        // in-place!

        auto evaluated = Q->apply(v);

        auto positive = elsa::maximum(evaluated, 0);

        Q->applyAdjoint(positive, out);
    }

    template <class data_t>
    SphericalPositivity<data_t>* SphericalPositivity<data_t>::cloneImpl() const
    {
        return new SphericalPositivity<data_t>{*Q};
    }

    template <class data_t>
    bool SphericalPositivity<data_t>::isEqual(const Functional<data_t>& other) const
    {
        if (!Functional<data_t>::isEqual(other))
            return false;

        if (!is<IndicatorNonNegativity<data_t>>(other))
            return false;

        const auto& otherFn = downcast<const SphericalPositivity<data_t>>(other);
        return *Q == *otherFn.Q;
    }

    template class SphericalPositivity<float>;
    template class SphericalPositivity<double>;
} // namespace elsa
