#include "DataDescriptor.h"
#include "DataContainer.h"
#include "Functional.h"
#include "OrthogonalComposition.h"
#include "elsaDefines.h"

namespace elsa
{

    template <class data_t>
    OrthogonalComposition<data_t>::OrthogonalComposition(const LinearOperator<data_t>& Q,
                                                         const Functional<data_t>& f)
        : Functional<data_t>{Q.getDomainDescriptor()}, Q{Q}, f{f.clone()}
    {
    }

    template <class data_t>
    data_t OrthogonalComposition<data_t>::evaluateImpl(const DataContainer<data_t>& Rx) const
    {
        auto Qx = Q.apply(Rx);

        return f->evaluate(Qx);
    }

    template <class data_t>
    void OrthogonalComposition<data_t>::getGradientImpl(const DataContainer<data_t>& Rx,
                                                        DataContainer<data_t>& out) const
    {
        auto Qx = Q.apply(Rx);

        Q.applyAdjoint(f->getGradient(Qx), out);
    }

    template <class data_t>
    LinearOperator<data_t>
        OrthogonalComposition<data_t>::getHessianImpl(const DataContainer<data_t>& Rx) const
    {
        return adjoint(Q) * f->getHessian(Q.apply(Rx)) * Q;
    }

    template <class data_t>
    DataContainer<data_t> OrthogonalComposition<data_t>::proximal(const DataContainer<data_t>& x,
                                                                  data_t t) const
    {
        DataContainer<data_t> out(x.getDataDescriptor());
        proximal(x, t, out);
        return out;
    }

    template <class data_t>
    void OrthogonalComposition<data_t>::proximal(const DataContainer<data_t>& x, data_t t,
                                                 DataContainer<data_t>& out) const
    {
        // P. L. Combettes and J.-C. Pesquet. “Proximal Splitting Methods in Signal Processing.” In:
        // Fixed-Point Algorithms for Inverse Problems in Science and Engineering.

        auto Qx = Q.apply(x);

        auto proxDelta = f->proximal(Qx, t) - Qx;

        Q.applyAdjoint(proxDelta, out);

        out += x;
    }

    template <class data_t>
    OrthogonalComposition<data_t>* OrthogonalComposition<data_t>::cloneImpl() const
    {
        return new OrthogonalComposition<data_t>{Q, *f};
    }

    template <class data_t>
    bool OrthogonalComposition<data_t>::isEqual(const Functional<data_t>& other) const
    {
        if (!Functional<data_t>::isEqual(other))
            return false;

        if (!is<OrthogonalComposition<data_t>>(other))
            return false;

        const auto& otherFn = downcast<const OrthogonalComposition<data_t>>(other);
        return Q == otherFn.Q && *f == *otherFn.f;
    }

    template class OrthogonalComposition<float>;
    template class OrthogonalComposition<double>;
} // namespace elsa
