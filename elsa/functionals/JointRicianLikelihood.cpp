#include "JointRicianLikelihood.h"
#include "DataContainer.h"
#include "TypeCasts.hpp"
#include "LinearOperator.h"
#include "BlockLinearOperator.h"
#include "Timer.h"
#include "Scaling.h"

namespace elsa
{

    /// construct a RandomBlockDescriptor to wrap up the absorption projection data
    /// and the axdt projection data as a single input for this functional
    RandomBlocksDescriptor mergeDescriptors(const DataDescriptor& desc1,
                                            const DataDescriptor& desc2)
    {
        std::vector<std::unique_ptr<DataDescriptor>> descs;

        descs.emplace_back(desc1.clone());
        descs.emplace_back(desc2.clone());

        return RandomBlocksDescriptor(descs);
    }

    template <typename data_t>
    RicianLoss<data_t>::RicianLoss(const DataContainer<data_t>& ffa,
                                   const DataContainer<data_t>& ffb, const DataContainer<data_t>& a,
                                   const DataContainer<data_t>& b,
                                   const LinearOperator<data_t>& absorp_op,
                                   const LinearOperator<data_t>& axdt_op, index_t N,
                                   bool approximate)
        : Functional<data_t>{mergeDescriptors(absorp_op.getDomainDescriptor(),
                                              axdt_op.getDomainDescriptor())},
          ffa_(ffa),
          ffb_(ffb),
          a_tilde_(a),
          b_tilde_(b),
          absorp_op_(absorp_op.clone()),
          axdt_op_(axdt_op.clone()),
          alpha_(ffb / ffa),
          log_ffa_(log(ffa)),
          N_(as<data_t>(N)),
          approximate_(approximate)
    {
    }

    template <typename data_t>
    bool RicianLoss<data_t>::isDifferentiable() const
    {
        return true;
    }

    template <typename data_t>
    data_t RicianLoss<data_t>::evaluateImpl(const DataContainer<data_t>& Rx) const
    {
        Timer timeguard("AXDTStatRecon", "evaluate");

        const auto mu = materialize(Rx.getBlock(0));
        const auto eta = materialize(Rx.getBlock(1));

        const auto log_d = -axdt_op_->apply(eta);
        const auto d = exp(log_d);

        auto log_a = -absorp_op_->apply(mu) + log_ffa_;
        auto a = exp(log_a);

        auto alpha_d = alpha_ * d;

        if (approximate_) {
            auto reorder = sq(a_tilde_ - a);
            reorder *= 2.0f;
            reorder += sq(b_tilde_ - (a * alpha_d));
            reorder /= a;

            return log_a.sum() + (N_ / 4.0f) * reorder.sum();
        } else {

            auto reorder = sq(a_tilde_);
            reorder *= 2.0f;
            reorder += sq(b_tilde_);
            reorder /= a;

            return 1.5f * log_a.sum() + (N_ * 0.5f) * a.sum()
                   + (N_ / 4.0f) * (reorder.sum() + a.dot(sq(alpha_d)))
                   - bessel_log_0((N_ / 2) * b_tilde_ * alpha_d).sum();
        }
    }

    template <typename data_t>
    void RicianLoss<data_t>::getGradientImpl(const DataContainer<data_t>& Rx,
                                             DataContainer<data_t>& out) const
    {
        Timer timeguard("AXDTStatRecon", "getGradient");

        const auto mu = materialize(Rx.getBlock(0));
        const auto eta = materialize(Rx.getBlock(1));

        const auto log_d = -axdt_op_->apply(eta);
        const auto d = exp(log_d);

        const auto log_a = -absorp_op_->apply(mu) + log_ffa_;
        const auto a = exp(log_a);
        // Some temporaries, potential improvement? First measure!
        const auto sqa = sq(a);
        const auto sqa_tilde = sq(a_tilde_);

        const auto alpha_d = alpha_ * d;

        auto tmp_a_alpha_d = a * alpha_d; // Might reuse this later in approximation codepath

        auto tmp_mu = 2.0f * (sqa - sqa_tilde);
        tmp_mu += sq(tmp_a_alpha_d);
        tmp_mu -= sq(b_tilde_);
        tmp_mu /= a;
        tmp_mu *= -N_ / 4.0f;
        tmp_mu -= approximate_ ? 1.0f : 1.5f; // only part that differs
        const auto grad_mu = absorp_op_->applyAdjoint(tmp_mu);

        const auto grad_eta = [&]() {
            if (approximate_) {
                tmp_a_alpha_d -= b_tilde_;
                tmp_a_alpha_d *= alpha_d;
                tmp_a_alpha_d *= -N_ * 0.5f;

                return axdt_op_->applyAdjoint(tmp_a_alpha_d);
            } else {
                auto tmp = a * sq(alpha_d);
                tmp *= -N_ * 0.5f;
                auto grad_eta_tmp_bessel = b_tilde_ * alpha_d;
                grad_eta_tmp_bessel *= N_ * 0.5f;
                tmp += grad_eta_tmp_bessel * bessel_1_0(grad_eta_tmp_bessel);

                return axdt_op_->applyAdjoint(tmp);
            }
        }();

        out.getBlock(0) = grad_mu;
        out.getBlock(1) = grad_eta;
    }

    template <typename data_t>
    LinearOperator<data_t> RicianLoss<data_t>::getHessianImpl(const DataContainer<data_t>& Rx) const
    {
        Timer timeguard("AXDTStatRecon", "getHessian");

        const auto mu = materialize(Rx.getBlock(0));
        const auto eta = materialize(Rx.getBlock(1));

        using BlockType = typename BlockLinearOperator<data_t>::BlockType;

        const auto d = exp(-axdt_op_->apply(eta));
        const auto a = exp(-absorp_op_->apply(mu)) * ffa_;

        const auto alpha_d = alpha_ * d;

        const auto H_1_1 =
            N_ * (2.f * sq(a) + 2.f * sq(a_tilde_) + (sq(a * alpha_d)) + sq(b_tilde_)) / 4.f / a;
        const auto H_1_2 = N_ * 0.5f * a * sq(alpha_d);
        const auto H_2_2 = [&]() {
            if (approximate_) {
                return (N_ * 0.5f) * (alpha_d * (2.f * a * alpha_d - b_tilde_));
            } else {
                auto tmp = N_ * a * sq(alpha_d);
                auto z = (N_ * 0.5f) * b_tilde_ * alpha_d;
                auto quot_z = bessel_1_0(z);
                tmp -= sq(z) * (1.f - sq(quot_z));
                return tmp;
            }
        }();

        auto hessian_absorp = [&]() {
            auto hessian_absorp_0 = adjoint(*absorp_op_) * H_1_1 * *absorp_op_;
            auto hessian_absorp_1 = adjoint(*absorp_op_) * H_1_2 * *axdt_op_;

            typename BlockLinearOperator<data_t>::OperatorList ops;
            ops.emplace_back(hessian_absorp_0.clone());
            ops.emplace_back(hessian_absorp_1.clone());
            return BlockLinearOperator<data_t>(ops, BlockType::COL).clone();
        }();

        auto hessian_axdt = [&]() {
            auto hessian_axdt_0 = adjoint(*axdt_op_) * H_1_2 * *absorp_op_;
            auto hessian_axdt_1 = adjoint(*axdt_op_) * H_2_2 * *axdt_op_;

            typename BlockLinearOperator<data_t>::OperatorList ops;
            ops.emplace_back(hessian_axdt_0.clone());
            ops.emplace_back(hessian_axdt_1.clone());
            return BlockLinearOperator<data_t>(ops, BlockType::COL).clone();
        }();

        typename BlockLinearOperator<data_t>::OperatorList ops;
        ops.emplace_back(std::move(hessian_absorp));
        ops.emplace_back(std::move(hessian_axdt));
        return leaf(BlockLinearOperator<data_t>(ops, BlockType::ROW));
    }

    template <typename data_t>
    RicianLoss<data_t>* RicianLoss<data_t>::cloneImpl() const
    {
        return new RicianLoss(ffa_, ffb_, a_tilde_, b_tilde_, *absorp_op_, *axdt_op_,
                              as<index_t>(N_), approximate_);
    }

    template <typename data_t>
    bool RicianLoss<data_t>::isEqual(const Functional<data_t>& other) const
    {
        if (!Functional<data_t>::isEqual(other))
            return false;

        auto otherFn = downcast_safe<RicianLoss>(&other);
        if (!otherFn)
            return false;

        if (otherFn->approximate_ != approximate_)
            return false;

        if (otherFn->ffa_ != ffa_ || otherFn->ffb_ != ffb_ || otherFn->a_tilde_ != a_tilde_
            || otherFn->b_tilde_ != b_tilde_ || *(otherFn->absorp_op_) != *(absorp_op_)
            || *(otherFn->axdt_op_) != *(axdt_op_) || otherFn->N_ != N_)
            return false;
        else
            return true;
    }

    // ------------------------------------------
    // explicit template instantiation
    template class RicianLoss<float>;
    template class RicianLoss<double>;
} // namespace elsa
