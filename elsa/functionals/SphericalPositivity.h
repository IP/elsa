#pragma once

#include "DataContainer.h"
#include "DataDescriptor.h"
#include "Functional.h"
#include "SphericalCoefficientsDescriptor.h"
#include "SphericalHarmonicsTransform.h"
#include "elsaDefines.h"

namespace elsa
{

    /**
     * @brief Non-negativity indicator in the spherical harmonics basis, using an arbitrary number
     * of sampling directions.
     *
     * This class represents a functional that serves as a non-negativity indicator in the spherical
     * harmonics basis.
     */
    template <class data_t>
    class SphericalPositivity final : public Functional<data_t>
    {

    public:
        /**
         * @brief Construct indicator functional from a SphericalCoefficientsDescriptor and a list
         * of sampling directions.
         *
         * @param domainDescriptro The domain descriptor for the spherical coefficients.
         * @param directions The list of directions.
         */
        SphericalPositivity(const SphericalCoefficientsDescriptor& domainDescriptor,
                            const axdt::DirVecList<data_t>& directions);

        /**
         * @brief Directly construct indicator functional from the basis change linear operator.
         * @param Q The SphericalHarmonicsTransform object.
         */
        explicit SphericalPositivity(const axdt::SphericalHarmonicsTransform<data_t>& Q);

        /**
         * @brief Check if the functional is prox-friendly.
         * @return True, as this functional is approximately prox-friendly.
         */
        bool isProxFriendly() const override { return true; }

        /**
         * @brief Check if the functional is differentiable.
         * @return False, as this functional is not differentiable.
         */
        bool isDifferentiable() const override { return false; }

        /**
         * @brief Compute the convex conjugate of the functional.
         * @param x The data container.
         * @return The convex conjugate of the functional.
         */
        data_t convexConjugate(const DataContainer<data_t>& x) const override;

        /**
         * @brief Compute the proximal operator of the functional.
         * We make use of the fact that semiorthogonal matrices "commute" with applying the
         * proximal, so we can project forwards with Q, clip the intensities and project back with
         * Q^T.
         *
         * @param v The data container.
         * @param t The irrelevant scalar value.
         * @return The result of the proximal operation.
         */
        DataContainer<data_t> proximal(const DataContainer<data_t>& v, data_t /*t*/) const override;

        /**
         * @brief Compute the proximal operator of the functional and store the result in the output
         * data container.
         *
         * @param v The data container.
         * @param t The irrelevant scalar value.
         * @param out The output data container.
         */
        void proximal(const DataContainer<data_t>& v, data_t /*t*/,
                      DataContainer<data_t>& out) const override;

    private:
        /**
         * @brief Evaluate the indicator functional, \infty if any sampling direction gets a
         * negative scattering strength, else 0.
         *
         * @param Rx The functional's argument.
         * @return The evaluated value of the functional.
         */
        data_t evaluateImpl(const DataContainer<data_t>& Rx) const override;

        /**
         * @brief As we're not differentiable, we throw an exception here.
         *
         * @param Rx The data container.
         * @param The output data container for the gradient.
         */
        void getGradientImpl(const DataContainer<data_t>& Rx,
                             DataContainer<data_t>&) const override;

        /**
         * @brief Second derivatives don't exist neither.
         *
         * @param Rx The data container.
         * @return The Hessian of the functional.
         */
        LinearOperator<data_t> getHessianImpl(const DataContainer<data_t>& Rx) const override;

        /**
         * @brief Clone the SphericalPositivity object.
         *
         * This function overrides the base class function and creates a copy of the
         * SphericalPositivity object.
         *
         * @return A pointer to the cloned SphericalPositivity object.
         */
        SphericalPositivity<data_t>* cloneImpl() const override;

        /**
         * @brief Check if the SphericalPositivity object is equal to another Functional object.
         *
         * This function overrides the base class function and checks if the SphericalPositivity
         * object is equal to another Functional object.
         *
         * @param other The other Functional object.
         * @return True if the objects are equal, false otherwise.
         */
        bool isEqual(const Functional<data_t>& other) const override;

        // The evaluation operator in the spherical harmonics basis
        // Named Q here to hint at its approximate semi-orthogonality
        std::unique_ptr<axdt::SphericalHarmonicsTransform<data_t>> Q;
    };

} // namespace elsa
