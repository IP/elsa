#pragma once

#include "DataContainer.h"
#include "DataDescriptor.h"
#include "Functional.h"
#include "LinearOperator.h"
#include "elsaDefines.h"

namespace elsa
{
    /// @brief Class modelling the composition of a functional with an (semi-)orthogonal
    /// LinearOperator Q (QQ^T = I): f \circ Q.
    /// Qs (semi-)orthogonality allows for efficient computation of the proximal operator of the
    /// composition: prox_{\lambda f \circ Q}(v) = x + Q^T (prox_{\lambda f}(Qv) -Qv) cf. P. L.
    /// For truly orthogonal Q this could simplified to: prox_{\lambda f \circ Q}(v) = Q^T
    /// prox_{\lambda f}(Qv), hence this implementation is inefficient in these cases.

    /// Combettes and J.-C. Pesquet. “Proximal Splitting Methods in Signal
    /// Processing.” In: Fixed-Point Algorithms for Inverse Problems in Science and Engineering.
    template <class data_t>
    class OrthogonalComposition : public Functional<data_t>
    {

    public:
        OrthogonalComposition(const LinearOperator<data_t>& Q, const Functional<data_t>& f);

        /**
         * @brief Check if the functional is prox-friendly.
         * @return True, as this functional is approximately prox-friendly.
         */
        bool isProxFriendly() const override { return f->isProxFriendly(); }

        /**
         * @brief Check if the functional is differentiable.
         * @return False, as this functional is not differentiable.
         */
        bool isDifferentiable() const override { return f->isDifferentiable(); }

        /**
         * @brief Compute the proximal operator of the functional.
         * We make use of the fact that semiorthogonal matrices "commute" with applying the
         * proximal, so we can project forwards with Q, apply prox_{\lambda_f}* project back with
         * Q^T.
         *
         * @param v The data container.
         * @param t The scalar value.
         * @return The result of the proximal operation.
         */
        DataContainer<data_t> proximal(const DataContainer<data_t>& v, data_t t) const override;

        /**
         * @brief Compute the proximal operator of the functional and store the result in the output
         * data container.
         *
         * @param v The data container.
         * @param t The irrelevant scalar value.
         * @param out The output data container.
         */
        void proximal(const DataContainer<data_t>& v, data_t t,
                      DataContainer<data_t>& out) const override;

    private:
        /**
         * @brief Evaluate the indicator functional, \infty if any sampling direction gets a
         * negative scattering strength, else 0.
         *
         * @param Rx The functional's argument.
         * @return The evaluated value of the functional.
         */
        data_t evaluateImpl(const DataContainer<data_t>& Rx) const override;

        /**
         * @brief As we're not differentiable, we throw an exception here.
         *
         * @param Rx The data container.
         * @param The output data container for the gradient.
         */
        void getGradientImpl(const DataContainer<data_t>& Rx,
                             DataContainer<data_t>&) const override;

        /**
         * @brief Second derivatives don't exist neither.
         *
         * @param Rx The data container.
         * @return The Hessian of the functional.
         */
        LinearOperator<data_t> getHessianImpl(const DataContainer<data_t>& Rx) const override;

        /**
         * @brief Clone the OrthogonalComposition object.
         *
         * This function overrides the base class function and creates a copy of the
         * OrthogonalComposition object.
         *
         * @return A pointer to the cloned OrthogonalComposition object.
         */
        OrthogonalComposition<data_t>* cloneImpl() const override;

        /**
         * @brief Check if the OrthogonalComposition object is equal to another Functional object.
         *
         * This function overrides the base class function and checks if the OrthogonalComposition
         * object is equal to another Functional object.
         *
         * @param other The other Functional object.
         * @return True if the objects are equal, false otherwise.
         */
        bool isEqual(const Functional<data_t>& other) const override;

        LinearOperator<data_t> Q;
        std::unique_ptr<Functional<data_t>> f;
    };

} // namespace elsa
