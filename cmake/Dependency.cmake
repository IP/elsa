macro(add_eigen)
    if(SYSTEM_EIGEN)
        message(STATUS "Using system-wide Eigen library...")
        find_package(Eigen3 REQUIRED 3.4.0)
    else()
        CPMAddPackage(
            NAME eigen3
            GIT_REPOSITORY https://gitlab.com/libeigen/eigen
            GIT_TAG 3.4.0
            DOWNLOAD_ONLY YES # Eigen's CMakelists are not intended for library use
        )

        # Add the Eigen library target to link against
        if(eigen3_ADDED)
            message(STATUS "Using bundled Eigen version in ${eigen3_SOURCE_DIR}")
            # this target is set-up as a drop-in replacement for a system eigen package.
            set(EIGEN_BUNDLED_INSTALLDIR "elsa/Eigen")
            add_library(Eigen INTERFACE)
            add_library(Eigen3::Eigen ALIAS Eigen)

            target_include_directories(
                Eigen SYSTEM # include with -isystem so many warnings are swallowed!
                INTERFACE $<BUILD_INTERFACE:${eigen3_SOURCE_DIR}>
                          $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}/${EIGEN_BUNDLED_INSTALLDIR}>
            )

            # register eigen in the build tree
            if(ELSA_EXPORT_BUILD_TREE)
                export(TARGETS Eigen FILE "${CMAKE_CURRENT_BINARY_DIR}/elsa/eigen3Targets.cmake")
            endif()

            # install custom elsa
            if(ELSA_INSTALL)
                install(TARGETS Eigen EXPORT eigenExport)
                install(
                    EXPORT eigenExport
                    NAMESPACE Eigen3::
                    FILE "eigen3Targets.cmake"
                    DESTINATION ${INSTALL_CONFIG_DIR}
                )

                install(DIRECTORY "${eigen3_SOURCE_DIR}/Eigen/"
                        DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}/${EIGEN_BUNDLED_INSTALLDIR}"
                )
            endif()
        endif()
    endif()
endmacro()

macro(add_thrust)
    if(ELSA_CUDA_ENABLED OR SYSTEM_THRUST)
        message(STATUS "Using system-wide thrust library (e.g. provided by CUDA environment)...")
        find_package(
            Thrust
            REQUIRED
            CONFIG
            HINTS
            "${CMAKE_CUDA_TOOLKIT_INCLUDE_DIRECTORIES}/../lib/cmake/thrust"
            PATHS
            /opt/cuda/
            /usr/local/cuda/
        )
    else()
        CPMAddPackage(
            NAME thrust
            GITHUB_REPOSITORY NVIDIA/thrust
            GIT_TAG 1.17.2
            OPTIONS "THRUST_ENABLE_INSTALL_RULES ON"
        )
        # TODO: We should export and install thrust properly
    endif()

    if(ELSA_CUDA_ENABLED)
        set(THRUST_DEVICE_SYSTEM CUDA)
    else()
        if(OpenMP_CXX_FOUND)
            set(THRUST_DEVICE_SYSTEM OMP)
        else()
            set(THRUST_DEVICE_SYSTEM CPP)
        endif()
    endif()

    # Follow thrusts defaults, overwrite the by passing THRUST_[HOST|DEVICE]_SYSTEM to CMake
    thrust_create_target(elsa::Thrust FROM_OPTIONS)

    # CMake 3.25 introduced the `SYSTEM` properties for targets. It this silences warnings from the library. In this
    # case thrust, is just emitting many (for us) silly warnings. For users without CMake version >= 3.25, we still have
    # include guards silencing warnings from thrust, thou they might be leaky. see
    # https://cmake.org/cmake/help/latest/prop_tgt/SYSTEM.html#prop_tgt:SYSTEM
    if(${CMAKE_VERSION} VERSION_GREATER_EQUAL 3.25)
        set_target_properties(elsa::Thrust PROPERTIES SYSTEM TRUE)
    endif()

    thrust_is_cuda_system_found(THRUST_CUDA_FOUND)
    thrust_is_tbb_system_found(THRUST_TBB_FOUND)
    thrust_is_omp_system_found(THRUST_OMP_FOUND)
    thrust_is_cpp_system_found(THRUST_CPP_FOUND)
    thrust_update_system_found_flags()
endmacro()

macro(add_spdlog)
    if(SYSTEM_SPDLOG)
        message(STATUS "Using system-wide spdlog library...")
        find_package(spdlog REQUIRED)
    else()
        # setup bundled spdlog this already reads spdlog's cmake definitions
        CPMAddPackage(
            NAME spdlog
            GITHUB_REPOSITORY gabime/spdlog
            VERSION 1.10.0
            OPTIONS "SPDLOG_INSTALL ON"
        )
        message(STATUS "Using bundled spdlog version in ${spdlog_SOURCE_DIR}")

        # because spdlog didn't do that on its own, we export the target. this is just for the in-buildtree linking,
        # won't be installed. store in the bindir/elsa/ directory, which may be nested in other build directories
        # somehow.
        if(ELSA_EXPORT_BUILD_TREE)
            export(TARGETS spdlog_header_only spdlog FILE "${CMAKE_CURRENT_BINARY_DIR}/elsa/spdlogTargets.cmake")
        endif()
    endif()
endmacro()

macro(add_doctest)
    if(SYSTEM_DOCTEST)
        message(STATUS "Using system-wide doctest library...")
        find_package(doctest REQUIRED)
    else()
        CPMAddPackage(NAME doctest GITHUB_REPOSITORY onqtam/doctest GIT_TAG 2.4.7)

        # add the CMake modules for automatic test discovery
        set(CMAKE_MODULE_PATH "${doctest_SOURCE_DIR}/scripts/cmake" ${CMAKE_MODULE_PATH})
    endif()
endmacro()
